# Documentation - ADTF3 ROS2 Bridge

This Document describes the intended usage of the code generator script to create adtfplugins (Streaming Source / Sink) which provide a bridge between ROS2 and ADTF 3.x

## Prerequisites

### Python

* Version - depends on the installed ROS2-Version
* cogapp >= 2.5.1 (MIT)
* colorama >= 0.3.9 (BSD)
* prettytable >= 0.7.2 (BSD-3-Clause)

### ROS2
* How to install ROS2 <https://index.ros.org/doc/ros2/Installation/>
* You can use `ament` und `colcon` as build system define it within the `Settings`.

Only those ROS2 distributions are tested with the script.
<table>
  <tr>
    <th>ROS2 Version </th>
    <th>Buildsystem</th>
  </tr>
  <tr>
    <td>Ardent Apalone</td>
    <td>ament</td>
  </tr>
  <tr>
    <td>Bouncy Bolson</td>
    <td>colcon</td>
  </tr>
  </tr>
    <td>Crystal Clemmys</td>
    <td>colcon</td>
  </tr>
</table>

Make sure to use the correct build system for the corresponding ROS2 version, otherwise the installation of ROS2 can be corrputed.

### ADTF
* Version >= 3.3.1 (recommend latest)

### Compliler 
* Depends on the ROS2 Version, in general on Windows MSVC2017 (toolchain VC141) and on Linux the GCC-Version delivered with Ubuntu 18.04.

## Overview
<img src="./documentation/pics/ADTF_ROS2_codegen.png" alt="overview"/>

## First Steps

### System Service
The provided ADTF System Service must be built from its sources and can be found in `plugins/adtf_system_service`.
Copy the folder to `path_to_ros\ros2_ws\src\ros2` and adjust the `CMakeLists.txt` if necessary.


Afterwards use a VisualStudio command line(Windows)
```
  cd path_to_ros2
  install\local_setup.bat
```
or a terminal(Unix) with the provided commands
```
  cd path_to_ros2
  ./install/local_setup.bash
```

This activates the ROS2 environment within the terminal.
Afterwards the service can be installed using the following commands:

**ament**
```
ament build --only-package adtf_ros2_system_service
```

**colcon**
```
colcon build --packages-select adtf_ros2_system_service
```
It is possible you have to set the ADTF_DIR as environment variable.
After the compilation and install process the plugins can be found within `ADTF_DIR/bin/ros2`.

### Environment within ADTF
In Order to use ROS2 within ADTF 3.x the environment needs to be extended.

#### Windows 
Just use the ADTF Configuration Editor and navigate to System Editor >> Environment variables and add these entries.

* PYTHONPATH = path_to_ros\install\Lib\site-packages
* Path= path_to_ros\install\Scripts;path_to_ros\install\bin;$(Path)


#### Linux
For Linux the best way to use ROS2 within ADTF 3.x is to write a small launcher script which we than add as custom ADTF Launcher within the ADTF Configuration Editor.

```bash
#!/bin/bash
# starter script for ADTF with ROS2 environment

echo 'Running: '$0
echo 'Setting up ROS2 Environment with script '$1
source $1

echo 'Starting: '$3 
$2/bin/adtf_launcher --session $3 --console --run
```

We can store this script for exanmple within ADTF_DIR/bin/ros2/ros_launcher.sh and add it afterwards in the ADTF Configuration Editor.
Navigate to Tools >> Options >> Launchers and press the Add-Button.

Fill in the Dialog with the following:
* Name: LauncherWithRos2
* Command: xterm -hold -e path/to/your/starter/bash path/to/ros2/local_setup.bash $(ADTF_DIR) $(ADTF_SESSION_FILENAME) 
* Working Directory: $(ADTF_DIR) 

Afterwards you can launch any given Session from the ADTF Configuration Editor in a ROS2 environment

## Functions
The Python script can be launched with any terminal. The starting point will be the main menu.
<img src="./documentation/pics/main.gif" alt="main"/>

### Settings
When launching the script for the first time, you will be prompted to adjust the settings.
The settings are located within the submenu (5). After adjusting the necessary parameters you can start using the script.

<img src="./documentation/pics/settings.PNG" alt="settings"/>

**ros_path:**

Path to your local ROS2 installation for example `C:\dev\ros2`. Please don't use any overlays here the script needs to know the root install path.

**adtf_path:**

This setting describes the absolute path to your ADTF 3.x installation `C:\ADTF\3.x.y`

**adtf_plugin_install_path:**

With this setting you can define the path, in which the generated plugins will be installed for example `C:\ADTF\3.x.y\addons\adtf3_ros2_bridge\bin`.

**visual_studio_path:**

This setting is only relevant when using the script on a windows OS.
It should point to the VisualStudio-Commandline-Batch file which enables the usage if MSBuild within the current environment.
For Example `"C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat"`


**ros_subscriber_pkg:**

Defines the name of the ROS2 package under which the generated Subscriber/Streaming Source plugins are installed.
The default value `adtf_generated_subscriber` may be changed.

**ros_publisher_pkg:**

Defines the name of the ROS2 package under which the generated Publisher/Streaming Sink plugins are installed.
The default value `adtf_generated_publisher` may be changed.


**ros_msg_pkg:**

Defines the name of the ROS2 package under which the generated ROS2 messages are installed.
The default value `adtf_generated_msgs` may be changed.

**ros_build_command:**

This setting defines the command which is used to compile and install the generated plugins with the ROS2 buildsystem.

The command `colcon build --merge-install --packages-select` uses `colcon` as build system
with a merge installation of ROS2. The argument `--packages-select` is used to built exactly one defined package  
without it all packages within the ROS2 installation would be rebuilt.
Own CMake arguments can be used with the `--cmake-args` argument for example ` --cmake-args -DCMAKE_BUILD_TYPE=Debug`.

[ROS2 Ament](https://github.com/ros2/ros2/wiki/Ament-Tutorial)

[ROS2 Colcon](https://github.com/ros2/ros2/wiki/Colcon-Tutorial)

### Create Subscriber
With this functions you can create a ROS2 Subscriber/ADTF Streaming Source from a ROS2 message file(*.msg).
The available message files can be displayed with the submenu(6) `Show ROS2 msg files`.

<img src="./documentation/pics/show_msgs.PNG" alt="show_msgs"/>

**Example:**
From a message file `IntegerArray.msg` with the following content:
```
int32[100] data
```
a streamign source is generated with the following stream type:
```C++
StreamName = "IntegerArray"; 
DDL =
"<structs>"
  "<struct alignment=\"1\" name=\"IntegerArray\" version=\"1\">"
    "<element alignment=\"1\" arraysize=\"100\" byteorder=\"LE\" bytepos=\"0\" name=\"data\" type=\"tInt32\"/>"
  "</struct>"
"</structs>"
```
The generated streamign source sends a new sample everytime a data package is received over the ROS2 DDS network.
<img src="./documentation/pics/create_sub.gif" alt="create_sub"/>


### Create Publisher
This function generates ROS2 Publisher/ADTF Streaming Sink with a given DDL-Description as xml or string.


**Example:**
The following file *.xml (*.txt) with given content:
```XML
<structs>
  <struct alignment="1" name="tHeaderStruct" version="1">
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="0" name="ui32HeaderVal" type="tUInt32" />
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="4" name="f64HeaderVal" type="tFloat64" />
  </struct>
  <struct alignment="1" name="tNestedStruct" version="1">
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="0" name="sHeaderStruct" type="tHeaderStruct" />
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="12" name="sSimpleStruct" type="tSimpleStruct" />
  </struct>
  <struct alignment="1" name="tSimpleStruct" version="1">
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="0" name="ui8Val" type="tUInt8" />
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="1" name="ui16Val" type="tUInt16" />
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="3" name="ui32Val" type="tUInt32" />
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="7" name="i32Val" type="tInt32" />
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="11" name="i64Val" type="tInt64" />
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="19" name="f64Val" type="tFloat64" />
    <element alignment="1" arraysize="1" byteorder="LE" bytepos="27" name="f32Val" type="tFloat32" />
  </struct>
</structs>
```

Will generate the following ROS2 message files if not already present within you ROS2 installation.
```
TSimpleStruct.msg
uint8 ui8_val
uint16 ui16_val
uint32 ui32_val
int32 i32_val
int64 i64_val
float64 f64_val
float32 f32_val

THeaderStruct.msg
uint32 ui32_header_val
float64 f64_header_val

TNestedStruct.msg
THeaderStruct s_header_struct
TSimpleStruct s_simple_struct
```
After the ROS2 build system has generated all the neccessary files a second step will use these files to generate
a Publisher/Streaming Sinks. These plugins will publish with each reserve sample a ROS2 message with the corresponding data within the ROS2 DDS network.

<img src="./documentation/pics/create_pub.gif" alt="create_pub"/>

