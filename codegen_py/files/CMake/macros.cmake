###########################################################
# This function installs a ros2 subscriber as adtf plugin #
###########################################################
function(adtf_ros2_install_subscriber TARGET INSTALL_PATH)
    set(SOURCES 
        src/adtf_ros2_subscriber_base.h
        src/${TARGET}.cpp 
        src/${TARGET}.h)
    adtf_add_plugin(${TARGET} ${SOURCES})
    target_link_libraries(${TARGET} adtf::systemsdk adtf::filtersdk)
    ament_target_dependencies(${TARGET} ${ARGN})

    if(WIN32)
        set(PLUGIN_DESCRIPTION_GENERATOR adtf_plugin_description_generator.exe)
    else()
        set(PLUGIN_DESCRIPTION_GENERATOR adtf_plugin_description_generator)
    endif()

    adtf_install_filter(${TARGET} ${INSTALL_PATH})
    add_custom_command(TARGET ${TARGET} POST_BUILD COMMAND ${ADTF_DIR}/bin/${PLUGIN_DESCRIPTION_GENERATOR} -plugin=${INSTALL_PATH}/${TARGET}.adtfplugin -output=${INSTALL_PATH}/${TARGET}.plugindescription)
endfunction()

##########################################################
# This function installs a ros2 publisher as adtf plugin #
##########################################################
function(adtf_ros2_install_publisher TARGET INSTALL_PATH)
    set(SOURCES 
        src/adtf_ros2_publisher_base.h
        src/${TARGET}.cpp 
        src/${TARGET}.h)
    adtf_add_plugin(${TARGET} ${SOURCES})
    target_link_libraries(${TARGET} adtf::systemsdk adtf::filtersdk)
    ament_target_dependencies(${TARGET} ${ARGN})

    if(WIN32)
        set(PLUGIN_DESCRIPTION_GENERATOR adtf_plugin_description_generator.exe)
    else()
        set(PLUGIN_DESCRIPTION_GENERATOR adtf_plugin_description_generator)
    endif()

    adtf_install_filter(${TARGET} ${INSTALL_PATH})
    add_custom_command(TARGET ${TARGET} POST_BUILD COMMAND ${ADTF_DIR}/bin/${PLUGIN_DESCRIPTION_GENERATOR} -plugin=${INSTALL_PATH}/${TARGET}.adtfplugin -output=${INSTALL_PATH}/${TARGET}.plugindescription)
endfunction()
