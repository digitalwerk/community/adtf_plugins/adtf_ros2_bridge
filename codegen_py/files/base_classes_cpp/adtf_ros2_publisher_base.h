/**
 *
 * @file
 * Copyright &copy; Digitalwerk GmbH
 *
 */

#pragma once
#include <memory>
#include <map>
#include <a_utils_platform_inc.h>
#include <adtf_base.h>
#include <adtf_systemsdk.h>
#include <adtf_streaming3.h>
#include <rclcpp/rclcpp.hpp>


template<class T>
class cRos2SampleReader : public adtf::streaming::ant::cSampleReader
{

public:

	virtual ~cRos2SampleReader() = default;
	cRos2SampleReader(const tChar* strStreamName, const tChar* strDDL);

	tResult Push(const adtf::streaming::ant::IStreamItem& oStreamItem, tTimeStamp tmTime) override;
	tVoid Clear() override;
	tResult Pop(adtf::streaming::ant::IStreamItem& oStreamItem) override;

protected:

	typedef T msgType;

	tBool																m_bLogging;
    adtf_util::cString													m_strNodeName;
    adtf_util::cString													m_strTopicName;
    adtf_util::cString													m_strStreamName;
    adtf_util::cString													m_strDDL;

	std::map<adtf_util::cString, adtf_util::cVariant>					m_oMap;

	virtual tVoid Mapping(msgType &oMsg) = 0;
	virtual tVoid MakeMap() = 0;

private:

	std::shared_ptr<rclcpp::Publisher<msgType>>							m_pPublisher;
	std::shared_ptr<rclcpp::Node>										m_pNode;

	adtf::mediadescription::ant::cSampleCodecFactory					m_oCodecFactory;

	tVoid Logging(const std::pair<adtf_util::cString, adtf_util::cVariant> &oItem) const;
};


template<class T>
cRos2SampleReader<T>::cRos2SampleReader(const tChar* strStreamName, const tChar* strDDL)
{
    m_strStreamName = strStreamName;
    m_strDDL = strDDL;;
}

template<class T>
tResult cRos2SampleReader<T>::Push(const adtf::streaming::ant::IStreamItem& oStreamItem, tTimeStamp tmTime)
{
    if (!m_pNode)
    {
        m_pNode = std::make_shared<rclcpp::Node>(m_strNodeName.GetPtr());
        m_pPublisher = m_pNode->create_publisher<msgType>(m_strTopicName.GetPtr());
    }

    adtf::ucom::object_ptr<const adtf::streaming::ant::ISample> pSample;
    RETURN_IF_FAILED(oStreamItem.GetSample(pSample));

    adtf::ucom::object_ptr<adtf::streaming::ant::IStreamType> pType;
    RETURN_IF_FAILED(adtf::mediadescription::ant::create_adtf_default_stream_type(m_strStreamName, m_strDDL, pType, m_oCodecFactory));

    auto oDecoder = m_oCodecFactory.MakeDecoderFor(*pSample);
    RETURN_IF_FAILED(oDecoder.IsValid());

    for (auto &oItem : m_oMap)
    {
        oItem.second = ddl::access_element::get_value(oDecoder, oItem.first);
        if (m_bLogging)
        {
            Logging(oItem);
        }
    }

    msgType oMsg;
    Mapping(oMsg);
    m_pPublisher->publish(oMsg);

    RETURN_NOERROR;
}

template<class T>
tVoid cRos2SampleReader<T>::Clear()
{
    return tVoid();
}

template<class T>
tResult cRos2SampleReader<T>::Pop(adtf::streaming::ant::IStreamItem& oStreamItem)
{
    return tResult();
}

template<class T>
tVoid cRos2SampleReader<T>::Logging(const std::pair<adtf_util::cString, adtf_util::cVariant> &oItem) const
{
    if (oItem.second.IsBool())
    {
        LOG_INFO(adtf_util::cString::Format("Publisher '%s' sends: '%s: %i'",m_strNodeName.GetPtr(), oItem.first.GetPtr(), oItem.second.GetBool()).GetPtr());
    }
    else if (oItem.second.IsInt())
    {
        LOG_INFO(adtf_util::cString::Format("Publisher '%s' sends: '%s: %i'",m_strNodeName.GetPtr(), oItem.first.GetPtr(), oItem.second.GetInt()).GetPtr());
    }
    else if (oItem.second.IsFloat())
    {
        LOG_INFO(adtf_util::cString::Format("Publisher '%s' sends: '%s: %f'",m_strNodeName.GetPtr(), oItem.first.GetPtr(), oItem.second.GetFloat()).GetPtr());
    }
    else if (oItem.second.IsString())
    {
        LOG_INFO(adtf_util::cString::Format("Publisher '%s' sends: '%s: %s'",m_strNodeName.GetPtr(), oItem.first.GetPtr(), oItem.second.GetStringPtr()).GetPtr());
    }
}



template<class T>
class cRos2PublisherBase :
    public adtf::streaming::catwo::cSampleStreamingSink,
    public cRos2SampleReader<T>
{

public:
	//redef because GCC 5.4
	typedef typename cRos2SampleReader<T>::msgType msgType;

	virtual ~cRos2PublisherBase() = default;
	cRos2PublisherBase(const tChar* strStreamName, const tChar* strDDL);

	tResult Construct() override;
	tResult Destruct() override;
	tResult StartStreaming() override;
	tResult StopStreaming() override;

protected:

	virtual tVoid Mapping(msgType &oMsg) = 0;
	virtual tVoid MakeMap() = 0;

private:

    adtf::base::ant::property_variable<adtf_util::cString>				m_propNodeName;
    adtf::base::ant::property_variable<adtf_util::cString>				m_propTopicName;
    adtf::base::ant::property_variable<tBool>							m_propLogging;

};

template<class T>
cRos2PublisherBase<T>::cRos2PublisherBase(const tChar* strStreamName, const tChar* strDDL) : cRos2SampleReader<T>(strStreamName, strDDL)
{
    adtf_util::cString strTmp = "";
    strTmp = "adtf_topic_";
    strTmp.Append(cRos2SampleReader<T>::m_strStreamName);
	m_propTopicName = adtf_util::cString(strTmp);

    strTmp = "adtf_publisher_";
    strTmp.Append(cRos2SampleReader<T>::m_strStreamName);
	m_propNodeName = adtf_util::cString(strTmp);

    RegisterPropertyVariable("Node name", m_propNodeName);
    RegisterPropertyVariable("Topic name", m_propTopicName);
    RegisterPropertyVariable("Logging", m_propLogging);

}

template<class T>
tResult cRos2PublisherBase<T>::Construct()
{
    MakeMap();

    RETURN_IF_FAILED(cSampleStreamingSink::Construct());

    cRos2SampleReader<T>::m_strNodeName = m_propNodeName;
    cRos2SampleReader<T>::m_strTopicName = m_propTopicName;
    cRos2SampleReader<T>::m_bLogging = m_propLogging;

    adtf::ucom::object_ptr<adtf::streaming::ant::IStreamType> pType;
    RETURN_IF_FAILED(adtf::mediadescription::ant::create_adtf_default_stream_type(cRos2SampleReader<T>::m_strStreamName, cRos2SampleReader<T>::m_strDDL, pType));
    RETURN_IF_FAILED(adtf::streaming::ant::create_pin(*this, *this, "in", pType));

    RETURN_NOERROR;
}

template<class T>
tResult cRos2PublisherBase<T>::Destruct()
{
    RETURN_IF_FAILED(cSampleStreamingSink::Destruct());
    RETURN_NOERROR;
}

template<class T>
tResult cRos2PublisherBase<T>::StartStreaming()
{
    RETURN_IF_FAILED(cSampleStreamingSink::StartStreaming());
    RETURN_NOERROR;
}

template<class T>
tResult cRos2PublisherBase<T>::StopStreaming()
{
    RETURN_IF_FAILED(cSampleStreamingSink::StopStreaming());
    RETURN_NOERROR;
}