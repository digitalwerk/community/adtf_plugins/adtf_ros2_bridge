/**
 *
 * @file
 * Copyright &copy; Digitalwerk GmbH
 *
 */

#pragma once
#include <memory>
#include <map>
#include <a_utils_platform_inc.h>
#include <adtf_base.h>
#include <adtf_systemsdk.h>
#include <adtf_streaming3.h>
#include <rclcpp/rclcpp.hpp>


using std::placeholders::_1;

template<class T>
class cRos2SubscriberBase : public adtf::streaming::catwo::cSampleStreamingSource
{

public:
	virtual ~cRos2SubscriberBase() = default;
	cRos2SubscriberBase(const tChar* strStreamName, const tChar* strDDL);

	tResult Construct() override;
	tResult Destruct() override;
	tResult StartStreaming() override;
	tResult StopStreaming() override;

protected:

	typedef T msgType;
	typedef typename T::SharedPtr msgSharedPtrType;

	virtual tVoid Mapping(const msgSharedPtrType pMsg) = 0;
	virtual tVoid MakeMap() = 0;

	std::map<adtf_util::cString, adtf_util::cVariant>					m_oMap;

private:

	adtf::base::ant::property_variable<adtf_util::cString>				m_propNodeName;
    adtf::base::ant::property_variable<adtf_util::cString>				m_propTopicName;
    adtf::base::ant::property_variable<tBool>							m_propLogging;

    tBool																m_bLogging;
    adtf_util::cString													m_strNodeName;
    adtf_util::cString												    m_strTopicName;
    adtf_util::cString													m_strStreamName;
    adtf_util::cString													m_strDDL;

	std::shared_ptr<rclcpp::Node>										m_pNode;
	std::shared_ptr<rclcpp::Subscription<msgType>>						m_pSubscription;
	adtf::ucom::object_ptr<adtf::services::ant::IReferenceClock>		m_pClock;

	std::unique_ptr<adtf::system::kernel_thread>						m_pThread;
    adtf::streaming::ant::cSampleWriter									m_oWriter;
    adtf::mediadescription::ant::cSampleCodecFactory					m_oCodecFactory;

	tVoid Callback(const msgSharedPtrType pMsg);
	tVoid Logging(const std::pair<adtf_util::cString, adtf_util::cVariant> &oItem) const;
};

template<class T>
cRos2SubscriberBase<T>::cRos2SubscriberBase(const tChar* strStreamName, const tChar* strDDL)
{

    m_strStreamName = strStreamName;
    m_strDDL = strDDL;

    adtf_util::cString strTmp = "";
    strTmp = "adtf_topic_";
    strTmp.Append(m_strStreamName);
    m_propTopicName = adtf_util::cString(strTmp);

    strTmp = "adtf_subscriber_";
    strTmp.Append(m_strStreamName);
    m_propNodeName = adtf_util::cString(strTmp);

    RegisterPropertyVariable("Node name", m_propNodeName);
    RegisterPropertyVariable("Topic name", m_propTopicName);
    RegisterPropertyVariable("Logging", m_propLogging);

}

template<class T>
tResult cRos2SubscriberBase<T>::Construct()
{
    MakeMap();

    RETURN_IF_FAILED(cSampleStreamingSource::Construct());
    RETURN_IF_FAILED(_runtime->GetObject(m_pClock));

    m_strNodeName = m_propNodeName;
    m_strTopicName = m_propTopicName;
    m_bLogging = m_propLogging;

    adtf::ucom::object_ptr<adtf::streaming::ant::IStreamType> pType;
    RETURN_IF_FAILED(adtf::mediadescription::ant::create_adtf_default_stream_type(m_strStreamName, m_strDDL, pType, m_oCodecFactory));
    RETURN_IF_FAILED(adtf::streaming::ant::create_pin(*this, m_oWriter, "out", pType));

    RETURN_NOERROR;
}

template<class T>
tResult cRos2SubscriberBase<T>::Destruct()
{
    RETURN_IF_FAILED(cSampleStreamingSource::Destruct());
    RETURN_NOERROR;
}

template<class T>
tResult cRos2SubscriberBase<T>::StartStreaming()
{
    RETURN_IF_FAILED(cSampleStreamingSource::StartStreaming());

    if (!m_pNode)
    {
        m_pNode = std::make_shared<rclcpp::Node>(m_strNodeName.GetPtr());
        m_pSubscription = m_pNode->create_subscription<msgType>(
            m_strTopicName.GetPtr(),
            std::bind(&cRos2SubscriberBase::Callback, this, _1)
            );
    }

    if (!m_pThread)
    {
        auto strThreadName = adtf_util::cString("ros_subscriber_thread_");
        strThreadName.Append(m_strNodeName.GetPtr());
        m_pThread = std::make_unique<adtf::system::kernel_thread>(strThreadName, [this]() {rclcpp::spin(m_pNode); });
    }


    RETURN_NOERROR;
}

template<class T>
tResult cRos2SubscriberBase<T>::StopStreaming()
{
    RETURN_IF_FAILED(cSampleStreamingSource::StopStreaming());
    m_pThread.reset(nullptr);
    RETURN_NOERROR;
}

template<class T>
tVoid cRos2SubscriberBase<T>::Callback(const msgSharedPtrType pMsg)
{
    Mapping(pMsg);

    adtf::ucom::object_ptr<adtf::streaming::ant::ISample> pNewSample;
    tResult nResult = adtf::streaming::ant::alloc_sample(pNewSample, m_pClock->GetStreamTime());
    if (nResult.IsFailed()) return;

    auto oCodec = m_oCodecFactory.MakeCodecFor(pNewSample);

    for (auto &oItem : m_oMap)
    {
        ddl::access_element::set_value(oCodec, oItem.first, oItem.second);

        if (m_bLogging)
        {
            Logging(oItem);
        }
    }

    m_oWriter << pNewSample << adtf::streaming::ant::trigger;
}

template<class T>
tVoid cRos2SubscriberBase<T>::Logging(const std::pair<adtf_util::cString, adtf_util::cVariant> &oItem) const
{
    if (oItem.second.IsBool())
    {
        LOG_INFO(adtf_util::cString::Format("Subscriber '%s' heard: '%s: %i'", m_strNodeName.GetPtr(), oItem.first.GetPtr(), oItem.second.GetBool()).GetPtr());
    }
    else if (oItem.second.IsInt())
    {
        LOG_INFO(adtf_util::cString::Format("Subscriber '%s' heard: '%s: %i'", m_strNodeName.GetPtr(), oItem.first.GetPtr(), oItem.second.GetInt()).GetPtr());
    }
    else if (oItem.second.IsFloat())
    {
        LOG_INFO(adtf_util::cString::Format("Subscriber '%s' heard: '%s: %f'", m_strNodeName.GetPtr(), oItem.first.GetPtr(), oItem.second.GetFloat()).GetPtr());
    }
    else if (oItem.second.IsString())
    {
        LOG_INFO(adtf_util::cString::Format("Subscriber '%s' heard: '%s: %s'", m_strNodeName.GetPtr(), oItem.first.GetPtr(), oItem.second.GetStringPtr()).GetPtr());
    }
}