import subprocess
import sys
from glob import glob

from colorama import Back, Fore, deinit, init
from prettytable import PrettyTable

from src.CLI import Menu as Cli
from src.CLI.Input import *
from src.Parser import *
from src.Parser.Helper import *
from src.templates import *
from src.Util.Settings import *
from src.Util.Temp import *


# some global vars
current_settings = None
working_dir = os.path.dirname(os.path.realpath(__file__))


# some reused constants defined here
class GLOBAL(ValueEnum):
    EXIT = 666
    V001 = "0.0.1"
    TMP_DIR = "tmp"
    SRC_DIR = "src"
    MSG_DIR = "msg"
    GENERATED_FILES_DIR = "generated"
    PATH_TO_BASE_CLS = "files" + os.sep + "base_classes_cpp"
    TEMPLATE_SUBSCRIBER_HEADER = "template_subscriber.h"
    TEMPLATE_SUBSCRIBER_CPP = "template_subscriber.cpp"
    TEMPLATE_PUBLISHER_HEADER = "template_subscriber.h"
    TEMPLATE_PUBLISHER_CPP = "template_subscriber.cpp"
    TEMPLATE_PACKAGE_XML = "template_package.xml"
    TEMPLATE_CMAKELISTS_TXT = "template_CMakeLists.txt"
    SUBSCRIBER_BASE = "adtf_ros2_subscriber_base.h"
    PUBLISHER_BASE = "adtf_ros2_publisher_base.h"
    SUBSCRIBER_BASE_NAME = "adtf_ros2_subscriber_"
    PUBLISHER_BASE_NAME = "adtf_ros2_publisher_"
    PACKCAGE_XML = "package.xml"
    CMAKELISTS_TXT = "CMakeLists.txt"
    MACROS_CMAKE = "Macros.cmake"
    JSON_FROM_MSG = "from_msg.json"
    JSON_FROM_DDL = "from_ddl.json"
    EXPORT_COG_VAR = "export_file"
    XML_VERSION_TAG = "version"
    XML_BUILD_DEPEND_TAG = "build_depend"
    XML_EXEC_DEPEND_TAG = "exec_depend"
    COGFILES_TXT = "cogfiles.txt"
    SETUP_BATCH = "setup.bat"
    SETUP_BASH = "setup.bash"
    STANDARD_BUILD_DEPENDS = ["rclcpp", "rcutils", "rmw_implementation_cmake", "rmw", "std_msgs"]
    STANDARD_EXEC_DEPENDS = ["rclcpp", "rcutils", "rmw_implementation_cmake", "rmw", "rosidl_default_runtime", "std_msgs"]
    MSG_BUILD_DEPENDS_IGNORE = ["ament_cmake", "rosidl_default_generators"]


def close():
    """
    This function is used for a managed exit of the application
    """
    Cli.Menu.clear()
    print(Back.CYAN + "        Bye!        ")
    deinit()
    sys.exit(GLOBAL.EXIT)


def show_help():
    """
    Function to show a simple help string
    """
    print(
        "\n" + Fore.CYAN +
        "{:<30}\n".format("Create Subscriber:") + Fore.WHITE +
        "with given ROS2 msg file this feature creates a ADTF-Plugin.\n" +
        "The plugin will be Streaming Source with a sample type matching the \n" +
        "given msg file. Be sure to check the limitations!\n\n" + Fore.CYAN +

        "{:<30}\n".format("Create Publisher:") + Fore.WHITE +
        "with given DDL xml file this feature creates a ADTF-Plugin.\n" +
        "the plugin will be Streaming Sink publishing data matching the\n" +
        "given xml file. Be sure to check the limitations!\n\n" + Fore.CYAN +

        "{:<30}\n".format("Create Message:") + Fore.WHITE +
        "with given DDL xml file this feature creates a ROS2 Message.\n\n" + Fore.CYAN +

        "{:<30}\n".format("Settings:") + Fore.WHITE +
        "Adjust the paths to your local ros2 and adtf src paths\n" +
        "and specifiy a plugin plugins location\n\n" + Fore.CYAN +

        "{:<30}\n".format("Show ROS2 msg files:") + Fore.WHITE +
        "Shows all the parsable msg files in ros2 dir defined in the settings.\n\n" + Fore.CYAN +

        "{:<30}\n".format("Help:") + Fore.WHITE +
        "Your reading it.\n\n" + Fore.CYAN +

        "{:<30}\n".format("Limitaions:") + Fore.WHITE +
        "- Only static arrays with a known size can be parsed\n" +
        "- Use char arrays with fixed size instead of string\n" +
        "- Initial values in a ROS2 msg file will be ignored\n" +
        "- Constant values in a ROS2 msg file can not be parsed\n\n"
    )


def show_settings():
    """
    shows all settings and values in a CLI table
    """
    table = PrettyTable(["Key", "Value"])
    table.align["Key"] = 'l'
    table.align["Value"] = 'l'
    for key, value in current_settings.items():
        table.add_row([key, value])
    print(table)

    
def edit_setting(setting, validator=None, post_action=None):
    """
    Edit a setting with this function

    :param setting: the setting to edit
    :type setting: any
    :param validator: add a validatotion for the new setting, defaults to None
    :type validator: function, optional
    :param post_action: add a postaction to modify the new setting, defaults to None
    :type post_action: function, optional
    """
    old_value = current_settings[setting]
    table = PrettyTable(["Setting", "Value"])
    table.align["Setting"] = 'l'
    table.align["Value"] = 'l'
    table.add_row([setting, old_value])
    print(table)
    new_value = input("New value: ")

    if validator:
        if not validator(new_value):
            print(Fore.RED + "%s is invalid!" % new_value)
            return

    if post_action:
        new_value = post_action(new_value)
    current_settings[setting] = new_value
    print(Fore.GREEN + "Value changed to %s" % new_value)


def show_parsable_msg_files():
    """
    Shows all parsable msg files in a table
    """
    table = PrettyTable(["msg file", "ros2 pkg", "path"])
    table.align["msg file"] = 'l'
    table.align["ros2 pkg"] = 'l'
    table.align["path"] = 'l'
    paths = get_all_msg_paths(current_settings[SETTINGS_CONST.ROS_SRC_PATH])
    msg_data = get_parsable_msgs(paths)
    for x in msg_data:
        table.add_row(x)
    print(table)


def cleanup_temp_files(dir_to_clean, *files_types):
    """
    function to delete spedific file types from given dir
    """
    delete_list = []

    for file_type in files_types:
        add_list = glob(dir_to_clean + os.sep + "*." + file_type)
        if add_list:
            delete_list += add_list

    for file in delete_list:
        os.remove(file)


def cleanup_temp_dirs():
    """
    Clean the tmp dir tree if it exists
    if the current dir is different to the tmp dir root 
    changes the cwd to root_dir
    """
    os.chdir(working_dir)
    if os.path.exists(os.path.join(working_dir, GLOBAL.TMP_DIR)):
        shutil.rmtree(os.path.join(working_dir, GLOBAL.TMP_DIR))


def create_temp_dirs():
    """
    Creates the tmp dir tree if needed
    """
    path = os.path.join(working_dir, GLOBAL.TMP_DIR)
    if os.path.exists(path):
        shutil.rmtree(path)
    # create a tmp dir
    create_dir_if_not_exist(path)
    create_dir_if_not_exist(os.path.join(path, GLOBAL.GENERATED_FILES_DIR))


def start_subscriber_creation():
    """
    creates a subscriber plugin with a given msg file
    catches all exceptions from called functions and
    cancels the process if needed
    """

    # enter msg file and validate it
    msg_file = input("Path to msg file: ")
    try:
        validate_path(msg_file)
    except FileNotFoundError:
        print(Fore.RED + "%s is invalid!" % msg_file)
        cleanup_temp_dirs()
        return

    # create temp dir structure
    create_temp_dirs()

    # parse msg file
    try:
        _ = MSG_Parser(
            current_settings[SETTINGS_CONST.ROS_SRC_PATH],
            msg_file,
            os.path.join(working_dir, GLOBAL.TMP_DIR),
            json=GLOBAL.JSON_FROM_MSG
        )

        print(Fore.GREEN + "\nDone Parsing: %s" % msg_file)
    except BaseException as e:
        print(Fore.RED + "\nFailed to parse: %s" % msg_file)
        print(e.args)
        cleanup_temp_dirs()
        return

    # reuse the generated json file and get the some info
    existing_build_depends = GLOBAL.STANDARD_BUILD_DEPENDS.copy()
    existing_exec_depend = GLOBAL.STANDARD_EXEC_DEPENDS.copy()

    with open(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.JSON_FROM_MSG)) as f:
        msg_data = json.load(f)

    name = msg_data[JSON_TAGS.MSG_NAME].lower()

    for x in msg_data[JSON_TAGS.MSG_DEPENDENCIES]:
        if x not in existing_build_depends:
            existing_build_depends.append(x)
        if x not in existing_exec_depend:
            existing_exec_depend.append(x)

    # create parent target directory not exist
    pkg_path_parent = os.path.join(
        current_settings[SETTINGS_CONST.ROS_SRC_PATH],
        ROS2.SRC,
        current_settings[SETTINGS_CONST.ROS_SUBSCRIBER_PKG])

    create_dir_if_not_exist(pkg_path_parent)
    pkg_path = os.path.join(pkg_path_parent, GLOBAL.SUBSCRIBER_BASE_NAME + name)

    # check if pkg already existst
    version = GLOBAL.V001
    if not os.path.exists(pkg_path):
        print(Fore.GREEN + "Package %s does not exist creating V %s" % (pkg_path, version))
        os.mkdir(pkg_path)
    else:
        try:
            version = check_existing_pkg(pkg_path)
        except:
            cleanup_temp_dirs()
            return

    # save the xml string for later
    xml_string = msg_data[JSON_TAGS.MSG_XML_STRING_CLEAN]

    # append new msg_data to json
    msg_data[JSON_TAGS.PKG_NAME] = GLOBAL.SUBSCRIBER_BASE_NAME + name
    msg_data[JSON_TAGS.PKG_VERSION] = version
    msg_data[JSON_TAGS.MSG_BUILD_DEPENDENCIES] = existing_build_depends
    msg_data[JSON_TAGS.MSG_EXEC_DEPENDENCIES] = existing_exec_depend
    msg_data[JSON_TAGS.ADTF_SRC_PATH] = current_settings[SETTINGS_CONST.ADTF_SRC_PATH]
    msg_data[JSON_TAGS.ADTF_PLUGIN_INSTALL_PATH] = current_settings[SETTINGS_CONST.ADTF_PLUGIN_INSTALL_PATH]

    # make a coglist.txt content
    header_file = GLOBAL.SUBSCRIBER_BASE_NAME + name + ".h"
    cpp_file = GLOBAL.SUBSCRIBER_BASE_NAME + name + ".cpp"

    cogfile = [
        GLOBAL.TEMPLATE_SUBSCRIBER_HEADER + " -d " + " -D " + GLOBAL.EXPORT_COG_VAR + "=" + GLOBAL.JSON_FROM_MSG + " -o " + GLOBAL.GENERATED_FILES_DIR + os.sep + header_file,
        GLOBAL.TEMPLATE_SUBSCRIBER_CPP + " -d " + " -D " + GLOBAL.EXPORT_COG_VAR + "=" + GLOBAL.JSON_FROM_MSG + " -o " + GLOBAL.GENERATED_FILES_DIR + os.sep + cpp_file,
        GLOBAL.TEMPLATE_PACKAGE_XML + " -D " + GLOBAL.EXPORT_COG_VAR + "=" + GLOBAL.JSON_FROM_MSG + " -o " + GLOBAL.GENERATED_FILES_DIR + os.sep + GLOBAL.PACKCAGE_XML,
        GLOBAL.TEMPLATE_CMAKELISTS_TXT + " -d " + " -D " + GLOBAL.EXPORT_COG_VAR + "=" + GLOBAL.JSON_FROM_MSG + " -o " + GLOBAL.GENERATED_FILES_DIR + os.sep + GLOBAL.CMAKELISTS_TXT
    ]

    # dump new added info to the json file
    # copy base class file
    # copy needed content to new files
    # create the coglist.txt
    try:
        make_json_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.JSON_FROM_MSG), **msg_data)
        shutil.copy2(os.path.join(working_dir, GLOBAL.PATH_TO_BASE_CLS, GLOBAL.SUBSCRIBER_BASE), os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR))
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.TEMPLATE_SUBSCRIBER_HEADER), "\n", subscriber_h.CONTENT),
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.TEMPLATE_SUBSCRIBER_CPP), "\n", subscriber_cpp.CONTENT)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.TEMPLATE_PACKAGE_XML), "\n", package_xml.CONTENT_PUB_SUB)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.TEMPLATE_CMAKELISTS_TXT), "\n", cmake.CONTENT_SUBSCRIBER)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.MACROS_CMAKE), "\n", cmake.FUNCTION_INSTALL_SUBSCRIBER)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.COGFILES_TXT), '\n', *cogfile)
    except IOError:
        print(Fore.RED + "Error while writing or copying files")
        cleanup_temp_dirs()
        return

    # run cog
    try:
        run_cog(msg_file)
    except:
        cleanup_temp_dirs()
        return

    # copy all files to target dir if existent
    pkg_path_src = os.path.join(pkg_path, GLOBAL.SRC_DIR)
    create_dir_if_not_exist(pkg_path_src)

    try:
        move_files(
            pkg_path,
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.PACKCAGE_XML),
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.MACROS_CMAKE),
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.CMAKELISTS_TXT)
        )
    except IOError:
        print(Fore.RED + "Error while moving files from %s to %s"
              % (os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR), pkg_path))
        cleanup_temp_dirs()
        return

    try:
        move_files(
            pkg_path_src,
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, header_file),
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, cpp_file),
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.SUBSCRIBER_BASE)
        )
    except IOError:
        print(Fore.RED + "Error while moving files from %s to %s"
              % (os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR), pkg_path_src))
        cleanup_temp_dirs()
        return

    # create a batch file and run it
    pkg_name = GLOBAL.SUBSCRIBER_BASE_NAME + name
    try:
        make_and_run_batch(pkg_name)
    except BaseException as e:
        print(Fore.RED + "Error while running install batch/bash.")
        cleanup_temp_dirs()
        return

    if version == GLOBAL.V001:
        print(
            Fore.GREEN + "Package %s succesfully installed V 0.0.1 \n\n"
            % current_settings[SETTINGS_CONST.ROS_SUBSCRIBER_PKG])
    else:
        print(
            Fore.GREEN + "Package %s succesfully updated to V %s \n\n"
            % (current_settings[SETTINGS_CONST.ROS_SUBSCRIBER_PKG], version))

    print(Fore.CYAN + "Here is your generated DDL as cString...\n")
    print(xml_string)
    print("\n\n")
    print(Fore.CYAN + "...and as xml\n")
    print(cString_to_xml(xml_string))
    print("\n\n")

    cleanup_temp_dirs()


def start_publisher_creation():
    """
    creates one or more publisher plugins with a given ddl file
    catches all exceptions from called functions and
    cancels the process if needed
    """

    # enter xml/txt file and validate it
    inpt = input("Path to xml/txt file: ")
    try:
        validate_path(inpt)
    except FileNotFoundError:
        print(Fore.RED + "%s is invalid!" % inpt)
        return

    # create temp dir structure
    create_temp_dirs()

    # parse input file
    try:
        _ = DDL_Parser(
            current_settings[SETTINGS_CONST.ROS_SRC_PATH],
            inpt,
            os.path.join(working_dir, GLOBAL.TMP_DIR),
            current_settings[SETTINGS_CONST.ROS_MSG_PKG],
            json=GLOBAL.JSON_FROM_DDL
        )

        print(Fore.GREEN + "\nDone Parsing: %s" % inpt)
    except BaseException as e:
        print(Fore.RED + "\nFailed to parse: %s" % inpt)
        print(e.args)
        cleanup_temp_dirs()
        return

    # check for possible publisher choices
    # we import the json file and give the user a choice
    with open(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.JSON_FROM_DDL)) as f:
        ddl_data = json.load(f)

    # now that the user made his choice we check it for dependencies
    # and make a list with the publishers we build and the msgs
    publisher_todo = make_user_choice_list(ddl_data)
    msg_todo = make_msg_todo_list(ddl_data, *publisher_todo)

    # first build all msgs derived from the todo_list
    if msg_todo:
        print(Fore.GREEN + "Building needed msg files:\n%s"
              % list_to_pretty_string(msg_todo))
        try:
            create_msgs(ddl_data, msg_todo)
        except BaseException as e:
            cleanup_temp_dirs()
            print(e.args)
            return
    else:
        print(Fore.GREEN + "No new msg files need to be generated")
    cleanup_temp_files(os.path.join(working_dir, GLOBAL.TMP_DIR), "txt", "bat", "msg", "xml")

    # now we can build each publisher in our todolist
    if publisher_todo:
        try:
            for pub in publisher_todo:
                create_publisher(ddl_data, pub)
        except BaseException as e:
            cleanup_temp_dirs()
            print(e)
            return

    print(Fore.GREEN + "All Publishers succesfully build: %s"
          % list_to_pretty_string(publisher_todo))

    cleanup_temp_dirs()


def start_message_creation():
    """
    creates one or more message files with a given ddl file
    catches all exceptions from called functions and
    cancels the process if needed
    """

    # enter xml/txt file and validate it
    inpt = input("Path to xml/txt file: ")
    try:
        validate_path(inpt)
    except FileNotFoundError:
        print(Fore.RED + "%s is invalid!" % inpt)
        return

    # create temp dir structure
    create_temp_dirs()

    # parse input file
    try:
        _ = DDL_Parser(
            current_settings[SETTINGS_CONST.ROS_SRC_PATH],
            inpt,
            os.path.join(working_dir, GLOBAL.TMP_DIR),
            current_settings[SETTINGS_CONST.ROS_MSG_PKG],
            json=GLOBAL.JSON_FROM_DDL
        )

        print(Fore.GREEN + "\nDone Parsing: %s" % inpt)
    except BaseException as e:
        print(Fore.RED + "\nFailed to parse: %s" % inpt)
        print(e.args)
        cleanup_temp_dirs()
        return

    # check for possible publisher choices
    # we import the json file and give the user a choice
    with open(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.JSON_FROM_DDL)) as f:
        ddl_data = json.load(f)

    # now that the user made his choice we check it for dependencies
    # and make a list with the publishers we build and the msgs
    msg_todo = make_user_choice_list(ddl_data)
    msg_todo = make_msg_todo_list(ddl_data, *msg_todo)

    print(Fore.GREEN + "Building needed msg files: %s"
          % list_to_pretty_string(msg_todo))
    try:
        create_msgs(ddl_data, msg_todo)
    except BaseException as e:
        cleanup_temp_dirs()
        print(e.args)
        return

    print(Fore.GREEN + "All Messages succesfully build: %s"
          % list_to_pretty_string(msg_todo))

    cleanup_temp_dirs()


def create_msgs(ddl_data: dict, msg_todo: list):
    """
    [summary]
    
    :param ddl_data: data from generated json
    :type ddl_data: dict
    :param msg_todo: list of msg files to generate
    :type msg_todo: list
    :exception raises multiple exceptions
    """
   
    # move the msg filee
    msg_files = glob(os.path.join(working_dir, GLOBAL.TMP_DIR) + os.sep + "*" + ROS_DOT_MSG_FILE)

    if msg_files:
        try:
            for file in msg_files:
                if file.split(".")[0].split(os.sep)[-1] in msg_todo:
                    shutil.move(file, os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR))
        except IOError:
            print(Fore.RED + "Error while moving files %s" % list_to_pretty_string(msg_files))
            raise

    # create parent target directory not exist
    msg_pkg_path = os.path.join(
        current_settings[SETTINGS_CONST.ROS_SRC_PATH],
        ROS2.SRC,
        current_settings[SETTINGS_CONST.ROS_MSG_PKG])

    # get the needed dependencies from the json data
    existing_build_depends = []
    existing_exec_depends = []
    new_msgs = []
    for key, value in ddl_data[JSON_TAGS.DDL_STRUCTS].items():
        if value[JSON_TAGS.IS_KNOWN] and value[JSON_TAGS.KNOWN_PKG] != current_settings[SETTINGS_CONST.ROS_MSG_PKG]:
            existing_build_depends.append(value[JSON_TAGS.KNOWN_PKG])
            existing_exec_depends.append(value[JSON_TAGS.KNOWN_PKG])
        else:
            new_msgs.append(key)

    # check if pkg already exists and manage the version
    version = GLOBAL.V001
    existing_msgs = []

    if not os.path.exists(msg_pkg_path):
        print(Fore.GREEN + "Package %s does not exist creating V %s" % (msg_pkg_path, version))
        os.mkdir(msg_pkg_path)
    else:
        try:
            version = check_existing_pkg(msg_pkg_path)
        except BaseException:
            raise

        # in order to build the pkg again we need to delete the build folder in the ros dir
        try:
            build_folder = os.path.join(
                current_settings[SETTINGS_CONST.ROS_SRC_PATH],
                ROS2.BUILD,
                current_settings[SETTINGS_CONST.ROS_MSG_PKG]
            )
            validate_path(build_folder)
            shutil.rmtree(build_folder)
        except FileNotFoundError:
            pass

        # we need to get the existing dependencies from the package xml and append it to our dependency list
        try:
            validate_path(os.path.join(msg_pkg_path, GLOBAL.PACKCAGE_XML))
        except FileNotFoundError:
            pass
        else:
            xml_tree = Et.parse(os.path.join(msg_pkg_path, GLOBAL.PACKCAGE_XML))
            xml_exec_depend = search_xml_elements(xml_tree.getroot(), GLOBAL.XML_EXEC_DEPEND_TAG)
            xml_build_depend = search_xml_elements(xml_tree.getroot(), GLOBAL.XML_BUILD_DEPEND_TAG)

            for x in xml_exec_depend:
                if x.text not in existing_exec_depends:
                    existing_exec_depends.append(x.text)

            for x in xml_build_depend:
                if x.text not in existing_build_depends and x.text not in GLOBAL.MSG_BUILD_DEPENDS_IGNORE:
                    existing_build_depends.append(x.text)

        # we need to check the existing CmakeLists
        try:
            validate_path(os.path.join(msg_pkg_path, GLOBAL.CMAKELISTS_TXT))
        except FileNotFoundError:
            pass
        else:
            existing_msgs = get_matching_lines_from_file(
                os.path.join(msg_pkg_path, GLOBAL.CMAKELISTS_TXT),
                ROS_DOT_MSG_FILE,
                trim=["\n", "\t", "\"", " ", "\\", "/", ROS_DOT_MSG_FILE, ROS_MSG_FILE]
            )
    # append new msgs to list if needed
    for msg in new_msgs:
        if msg not in existing_msgs:
            existing_msgs.append(msg)

    # append new data to json
    ddl_data[JSON_TAGS.MSG_FILES] = existing_msgs
    ddl_data[JSON_TAGS.PKG_NAME] = current_settings[SETTINGS_CONST.ROS_MSG_PKG]
    ddl_data[JSON_TAGS.PKG_VERSION] = version
    ddl_data[JSON_TAGS.MSG_BUILD_DEPENDENCIES] = existing_build_depends
    ddl_data[JSON_TAGS.MSG_EXEC_DEPENDENCIES] = existing_exec_depends

    # make a coglist.txt content
    cogfile = [
        GLOBAL.TEMPLATE_PACKAGE_XML + " -D " + GLOBAL.EXPORT_COG_VAR + "=" + GLOBAL.JSON_FROM_DDL + " -o " + GLOBAL.GENERATED_FILES_DIR + os.sep + GLOBAL.PACKCAGE_XML,
        GLOBAL.TEMPLATE_CMAKELISTS_TXT + " -d " + " -D " + GLOBAL.EXPORT_COG_VAR + "=" + GLOBAL.JSON_FROM_DDL + " -o " + GLOBAL.GENERATED_FILES_DIR + os.sep + GLOBAL.CMAKELISTS_TXT
    ]

    # push new added info to the json file
    # copy needed content to new files
    # create the coglist.txt
    try:
        make_json_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.JSON_FROM_DDL), **ddl_data)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.TEMPLATE_PACKAGE_XML), "\n", package_xml.CONTENT_MSG)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.TEMPLATE_CMAKELISTS_TXT), "\n", cmake.CONTENT_MSG)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.COGFILES_TXT), '\n', *cogfile)
    except IOError:
        print(Fore.RED + "Error while writing files")
        raise
    except BaseException as e:
        print(Fore.RED + "Unkown error while writing files to %s" % msg_pkg_path)
        print(e.args)
        raise

    # run cog
    run_cog(new_msgs)

    # copy all files to target dir if existent then ask if overwrite
    msg_dir = os.path.join(msg_pkg_path, GLOBAL.MSG_DIR)
    create_dir_if_not_exist(msg_dir)
    try:
        move_files(
            msg_pkg_path,
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.PACKCAGE_XML),
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.CMAKELISTS_TXT)
        )

        msg_files = glob(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR) + os.sep + "*" + ROS_DOT_MSG_FILE)
        if msg_files:
            move_files(msg_dir, *msg_files)

    except IOError:
        print(
            Fore.RED + "Error while moving files from %s to %s"
            % (os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR), msg_pkg_path)
        )
        raise

    # build and plugins the created msgs
    make_and_run_batch(current_settings[SETTINGS_CONST.ROS_MSG_PKG])

    # done
    if version == GLOBAL.V001:
        print(
            Fore.GREEN + "Package %s succesfully installed V0.0.1 \n\n"
            % current_settings[SETTINGS_CONST.ROS_MSG_PKG])
    else:
        print(
            Fore.GREEN + "Package %s succesfully updated to V%s \n\n"
            % (current_settings[SETTINGS_CONST.ROS_MSG_PKG], version))


def create_publisher(ddl_data: dict, pub_todo: str):
    """
    creates one publisher 

    :param ddl_data: data from generated json
    :type ddl_data: dict
    :param pub_todo: publisher to create
    :type pub_todo: str
    :raises MsgAmbiguousException: 
    :raises MsgNotFoundException:
    :raises MsgNotParsableException:
    :raises FileNotFoundError: 
    :raises BaseException:
    """

    print("Building Publisher for %s" % pub_todo)

    # first get abspath to msg file from pub_todo str
    msg_files = find_files(current_settings[SETTINGS_CONST.ROS_SRC_PATH], pub_todo + ROS_DOT_MSG_FILE)
    if len(msg_files) == 2:
        msg_file = msg_files[0]
    elif len(msg_files) > 2:
        raise MsgAmbiguousException("The files: %s are ambiguous" % str(msg_files))
    else:
        raise MsgNotFoundException("The file: %s could not be found" % str(msg_files))

    # parse msg file
    try:
        _ = MSG_Parser(
            current_settings[SETTINGS_CONST.ROS_SRC_PATH],
            msg_file,
            os.path.join(working_dir, GLOBAL.TMP_DIR),
            json=GLOBAL.JSON_FROM_MSG
        )

        print(Fore.GREEN + "\nDone Parsing: %s" % msg_file)
    except BaseException as e:
        print(Fore.RED + "\nFailed to parse: %s" % msg_file)
        print(e.args)
        raise

    # reuse the generated json file and get the some info
    existing_build_depends = GLOBAL.STANDARD_BUILD_DEPENDS.copy()
    existing_exec_depend = GLOBAL.STANDARD_EXEC_DEPENDS.copy()

    with open(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.JSON_FROM_MSG)) as f:
        msg_data = json.load(f)

    name = msg_data[JSON_TAGS.MSG_NAME].lower()

    for x in msg_data[JSON_TAGS.MSG_DEPENDENCIES]:
        if x not in existing_build_depends:
            existing_build_depends.append(x)
        if x not in existing_exec_depend:
            existing_exec_depend.append(x)

    # create parent target directory not exist
    pkg_path_parent = os.path.join(
        current_settings[SETTINGS_CONST.ROS_SRC_PATH],
        ROS2.SRC,
        current_settings[SETTINGS_CONST.ROS_PUBLISHER_PKG])

    create_dir_if_not_exist(pkg_path_parent)
    pkg_path = os.path.join(pkg_path_parent, GLOBAL.PUBLISHER_BASE_NAME + name)

    # check if pkg already existst
    version = GLOBAL.V001
    if not os.path.exists(pkg_path):
        print(Fore.GREEN + "Package %s does not exist creating V %s" % (pkg_path, version))
        os.mkdir(pkg_path)
    else:
        try:
            version = check_existing_pkg(pkg_path)
        except BaseException:
            raise

    # append new data to json
    msg_data[JSON_TAGS.MSG_DATA_PATHS] = ddl_data[JSON_TAGS.DDL_STRUCTS][msg_data[JSON_TAGS.MSG_NAME]][JSON_TAGS.MSG_DATA_PATHS]
    msg_data[JSON_TAGS.MSG_XML_STRING] = ddl_data[JSON_TAGS.DDL_STRUCTS][msg_data[JSON_TAGS.MSG_NAME]][JSON_TAGS.MSG_XML_STRING]
    msg_data[JSON_TAGS.MSG_XML_STRING_CLEAN] = ddl_data[JSON_TAGS.DDL_STRUCTS][msg_data[JSON_TAGS.MSG_NAME]][JSON_TAGS.MSG_XML_STRING_CLEAN]
    msg_data[JSON_TAGS.MSG_NAME] = ddl_data[JSON_TAGS.DDL_STRUCTS][msg_data[JSON_TAGS.MSG_NAME]][JSON_TAGS.STREAM_NAME]
    msg_data[JSON_TAGS.PKG_NAME] = GLOBAL.PUBLISHER_BASE_NAME + name
    msg_data[JSON_TAGS.PKG_VERSION] = version
    msg_data[JSON_TAGS.MSG_BUILD_DEPENDENCIES] = existing_build_depends
    msg_data[JSON_TAGS.MSG_EXEC_DEPENDENCIES] = existing_exec_depend
    msg_data[JSON_TAGS.ADTF_SRC_PATH] = current_settings[SETTINGS_CONST.ADTF_SRC_PATH]
    msg_data[JSON_TAGS.ADTF_PLUGIN_INSTALL_PATH] = current_settings[SETTINGS_CONST.ADTF_PLUGIN_INSTALL_PATH]

    # make a coglist.txt content
    header_file = GLOBAL.PUBLISHER_BASE_NAME + name + ".h"
    cpp_file = GLOBAL.PUBLISHER_BASE_NAME + name + ".cpp"

    cogfile = [
        GLOBAL.TEMPLATE_SUBSCRIBER_HEADER + " -d " + " -D " + GLOBAL.EXPORT_COG_VAR + "=" + GLOBAL.JSON_FROM_MSG + " -o " + GLOBAL.GENERATED_FILES_DIR + os.sep + header_file,
        GLOBAL.TEMPLATE_SUBSCRIBER_CPP + " -d " + " -D " + GLOBAL.EXPORT_COG_VAR + "=" + GLOBAL.JSON_FROM_MSG + " -o " + GLOBAL.GENERATED_FILES_DIR + os.sep + cpp_file,
        GLOBAL.TEMPLATE_PACKAGE_XML + " -D " + GLOBAL.EXPORT_COG_VAR + "=" + GLOBAL.JSON_FROM_MSG + " -o " + GLOBAL.GENERATED_FILES_DIR + os.sep + GLOBAL.PACKCAGE_XML,
        GLOBAL.TEMPLATE_CMAKELISTS_TXT + " -d " + " -D " + GLOBAL.EXPORT_COG_VAR + "=" + GLOBAL.JSON_FROM_MSG + " -o " + GLOBAL.GENERATED_FILES_DIR + os.sep + GLOBAL.CMAKELISTS_TXT
    ]

    # dump new added info to the json file
    # copy base class file
    # copy needed content to new files
    # create the coglist.txt
    try:
        make_json_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.JSON_FROM_MSG), **msg_data)
        shutil.copy2(os.path.join(working_dir, GLOBAL.PATH_TO_BASE_CLS, GLOBAL.PUBLISHER_BASE),
                     os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR))
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.TEMPLATE_PUBLISHER_HEADER), "\n", publisher_h.CONTENT)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.TEMPLATE_PUBLISHER_CPP), "\n", publisher_cpp.CONTENT)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.TEMPLATE_PACKAGE_XML), "\n", package_xml.CONTENT_PUB_SUB)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.TEMPLATE_CMAKELISTS_TXT), "\n", cmake.CONTENT_PUBLISHER)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.MACROS_CMAKE), "\n", cmake.FUNCTION_INSTALL_PUBLISHER)
        make_file(os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.COGFILES_TXT), '\n', *cogfile)
    except IOError:
        print(Fore.RED + "Error while writing or copying files")
        raise

    # run cog
    run_cog(msg_file)

    # copy all files to target dir if existent
    pkg_path_src = os.path.join(pkg_path, GLOBAL.SRC_DIR)
    create_dir_if_not_exist(pkg_path_src)

    try:
        move_files(
            pkg_path,
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.PACKCAGE_XML),
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.MACROS_CMAKE),
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.CMAKELISTS_TXT)
        )
    except IOError:
        print(Fore.RED + "Error while moving files from %s to %s"
              % (os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR), pkg_path))
        raise

    try:
        move_files(
            pkg_path_src,
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, header_file),
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, cpp_file),
            os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR, GLOBAL.PUBLISHER_BASE)
        )

    except IOError:
        print(
            Fore.RED + "Error while moving files from %s to %s"
            % (os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.GENERATED_FILES_DIR), pkg_path_src)
        )
        raise

    # create a batch file and run it
    pkg_name = GLOBAL.PUBLISHER_BASE_NAME + name
    make_and_run_batch(pkg_name)

    if version == GLOBAL.V001:
        print(
            Fore.GREEN + "Package %s succesfully installed V 0.0.1 \n\n"
            % current_settings[SETTINGS_CONST.ROS_PUBLISHER_PKG])
    else:
        print(
            Fore.GREEN + "Package %s succesfully updated to V %s \n\n"
            % (current_settings[SETTINGS_CONST.ROS_PUBLISHER_PKG], version))


def make_and_run_batch(pkg_name):
    """
    Generates and executes a batch/bash file from template

    :param pkg_name: the name of the ros pkg to build with the batch file
    :type pkg_name: str
    """

    # make file
    if os.name == "nt":
        vs_batch = current_settings[SETTINGS_CONST.VISUAL_STUDIO_BAT]
        ros_setup = os.path.join(current_settings[SETTINGS_CONST.ROS_SRC_PATH], ROS2.LOCAL_SETUP_WIN)
        ros_src_dir = current_settings[SETTINGS_CONST.ROS_SRC_PATH]
        ros_build_system = current_settings[SETTINGS_CONST.ROS_BUILD_COMMAND]
        content = get_batch(ros_setup, ros_src_dir, ros_build_system, pkg_name, vs_cmd_bat=vs_batch)
        path = os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.SETUP_BATCH)
    else:
        ros_setup = os.path.join(current_settings[SETTINGS_CONST.ROS_SRC_PATH], ROS2.LOCAL_SETUP_UNX)
        ros_src_dir = current_settings[SETTINGS_CONST.ROS_SRC_PATH]
        ros_build_system = current_settings[SETTINGS_CONST.ROS_BUILD_COMMAND]
        content = get_batch(ros_setup, ros_src_dir, ros_build_system, pkg_name)
        path = os.path.join(working_dir, GLOBAL.TMP_DIR, GLOBAL.SETUP_BASH)
    
    try:
        make_file(path, "\n", content)

        # on unix we need to set executable permissions
        if os.name == "posix":
            os.chmod(path, 0o777)

    except IOError:
        print(Fore.RED + "Error while creating %s" % path)
        raise

    # run file
    try:
        p_arg = subprocess.run(path, shell=True, check=True)
        p_arg.check_returncode()
    except subprocess.CalledProcessError:
        print(Fore.RED + "\nError while running build & plugins for %s" % pkg_name)
        raise


def run_cog(job):
    """
    changes the current dir to build files 
    with the cog codegenerator.
    After the subprocces os complete the dir is changed back to root_dir param

    :param job: the job running
    :raises subprocess.CalledProcessError: is raised when the subprocess returns non zero exit state 
    :raises Exception: is raised when the raises an unkown error
    """
    try:
        os.chdir(os.path.join(working_dir, GLOBAL.TMP_DIR))
        subprocess.run([sys.executable, "-m", "cogapp", "@" + GLOBAL.COGFILES_TXT])
        print(Fore.GREEN + "Files generated for: %s" % job)
        os.chdir(working_dir)
    except subprocess.CalledProcessError:
        print(Fore.RED + "Failed to generate files for: %s" % job)
        os.chdir(working_dir)
        raise
    except Exception as e:
        print(Fore.RED + "Failed to generate files for: %s" % job)
        print(e.args)
        os.chdir(working_dir)
        raise


def check_existing_pkg(pkg_path):
    """
    Checks for an existing ros package with given param pkg_path.
    If the package exists the version is incremented and returned.

    :param pkg_path: the abs path to the pkg
    :type pkg_path: str, pathlike
    :raises Et.ParseError: when the version tag cant be located or the file is corrupted
    :raises UserAbortException: if the user decides to abort the process
    :raises BaseException: unknown error
    """
    version = GLOBAL.V001
    # check for package xml and get the version
    path_to_existing_package_xml = os.path.join(pkg_path, GLOBAL.PACKCAGE_XML)

    if os.path.exists(path_to_existing_package_xml):
        try:
            xml_tree = Et.parse(path_to_existing_package_xml)

            # get version and incr it
            version_xml_child = search_xml_elements(xml_tree.getroot(), GLOBAL.XML_VERSION_TAG)
            if version_xml_child:
                old_version = version_xml_child[0].text
                new_version = "00" + str(int("".join(old_version.split('.'))) + 1)
                while len(new_version) > 3 and new_version[0] == "0":
                    new_version = new_version[1:]
                version = ".".join(list(new_version))

                print(Fore.GREEN + "Package %s exists already. Old version: %s, New Version: %s "
                      % (pkg_path, old_version, version))

                print(Fore.YELLOW + "Do you want to continue? Previous files will be overwritten (Y/N)")
                user_input_yes_no()

                return version
            else:
                raise Et.ParseError

        except Et.ParseError:
            print(Fore.RED + "The existing package.xml file: %s seems to be corrupted." % path_to_existing_package_xml)
            raise

        except UserAbortException:
            print(Fore.RED + "OK, aborted!")
            raise

        except BaseException as e:
            print(Fore.RED + "Unkown errror while parsing %s" % path_to_existing_package_xml)
            print(e.args)
            raise
    else:
        print(Fore.GREEN + "Package %s does seem to exist, but no package xml was found exist creating %s"
              % (pkg_path, version))
        return version


def make_user_choice_list(data: dict):
    """
    generates a list with the user choices respective to given data

    :param data: data from which to choose of
    :type data: dict
    :return: a list of all the chosen values
    :rtype: list
    """
    print("\nSelect the struct(s) you want to create convert 0, 1, ... n, comma seperated")

    choice_dict = {}
    for key, i in zip(data[JSON_TAGS.DDL_STRUCTS].keys(),
                      range(len(data[JSON_TAGS.DDL_STRUCTS].keys()))):
        choice_dict.update({str(i): key})
    for i, key in choice_dict.items():
        print(i, ":", key)

    choice = user_input_range(range(len(data[JSON_TAGS.DDL_STRUCTS].keys())),
                              Fore.YELLOW + "invalid selection!")

    for i, key in choice_dict.copy().items():
        if i not in choice:
            del choice_dict[i]

    return list(choice_dict.values())


def make_msg_todo_list(data: dict, *choice):
    """
    recursivly make a list of alle msgs to genrated with respect to their dependecies

    :param data: data from which to choose of
    :type data: dict
    :param choice: add params here to specifiy a subset, only msgs found in the given subset will be processed
    :return: a list of all msgs to built
    :rtype: list
    """
    result = []
    for key, value in data[JSON_TAGS.DDL_STRUCTS].items():
        if key not in choice:
            continue

        if not value[JSON_TAGS.IS_KNOWN]:
            result.append(key)

            if value[JSON_TAGS.DEPENDENCIES]:
                for v in value[JSON_TAGS.DEPENDENCIES]:
                    search_result = find(lambda item: item == v, data[JSON_TAGS.DDL_STRUCTS].keys())
                    if search_result:
                        rec_result = make_msg_todo_list(data, search_result)
                        if rec_result:
                                result.extend(rec_result)

    return list(set(result))


def regex_validator(regex, phrase):
    """
    helper function for settings validation

    :param regex: the regex to use
    :type regex: str, regex like
    :param phrase: the phrase to cheack with regex
    :type phrase: str
    :return: a True if ok else False
    :rtype: bool
    """    
    p = re.compile(regex)
    m = p.findall(phrase)
    return True if len(m) == len(phrase) and len(phrase) > 0 else False


if __name__ == "__main__":
    """
    Application main event loop
    """

    # Application main loop inside the settings scope
    try:
        with Settings(os.path.dirname(os.path.realpath(__file__))) as settings:

            # we make the current settings and the cwd global for easy use
            # we just overwrite over defined global vars at the top
            globals()["current_settings"] = settings
            globals()["working_dir"] = os.path.dirname(os.path.realpath(__file__))

            # setup the env
            init(autoreset=True)
            Cli.Menu.clear()

            # check the os
            if not (sys.platform.startswith("win") or sys.platform.startswith("linux")):
                print(Fore.RED + "\nOnly Windows and Ubuntu supported at this time!\n")
                sys.exit(GLOBAL.EXIT)

            # check if the settings are initial
            if settings == SETTINGS_CONST.INIT_SETTINGS:
                print(Fore.YELLOW + "\nSettings not set! Please go to the 'Settings' menu and adjust the values according to your system.\n")

            # create a simple cli menu
            settings_submenus = []
            for s, i in zip(current_settings.keys(), range(len(current_settings.keys()))):
                if i < 4:
                    settings_submenus.append(Cli.Menu(
                        str(i + 1),
                        "Edit " + s,
                        edit_setting,
                        [s, lambda item: True if os.path.exists(item) else False, lambda item: item + os.sep if not item.endswith(os.sep) else item]))
                elif i == 7:
                    settings_submenus.append(Cli.Menu(
                        str(i + 1),
                        "Edit " + s,
                        edit_setting,
                        [s]))
                else:
                    settings_submenus.append(Cli.Menu(
                        str(i + 1),
                        "Edit " + s,
                        edit_setting,
                        [s, lambda item: regex_validator("[a-zA-Z_]", item)]))

            main_menu = Cli.Menu('1', "Main Menu", sub_menus=[
                # add the main menu submenus
                Cli.Menu('2', "Create Subscriber", start_subscriber_creation),
                Cli.Menu('3', "Create Publisher", start_publisher_creation),
                Cli.Menu('4', "Create Message", start_message_creation),
                Cli.Menu('5', "Settings", show_settings, sub_menus=settings_submenus),
                Cli.Menu('6', "Show ROS2 msg files", show_parsable_msg_files),
                Cli.Menu('7', "Help", show_help),
                Cli.Menu('8', "Exit", close)]
            )

            # execute the menu event loop
            main_menu.run_menu()
            
    except SystemExit as ex:
        TempFileDistributor.clean_up()
        if GLOBAL.EXIT in ex.args:
            pass
        else:
            print(Fore.RED + "\nUnplanned exit")
            print(ex.args)

    except KeyboardInterrupt:
        TempFileDistributor.clean_up()
        print(Fore.YELLOW + "\nPressed CTRL+C")

    except IOError:
        print(Fore.RED + "\nCould not open settings file, or settings file is corrupted")

    except BaseException as ex:
        TempFileDistributor.clean_up()
        print(Fore.RED + "\nUnkown error")
        print(ex.args)
