def user_input_yes_no():
    """
    Simples yes/no user input cli handler
    """
    while True:
        var = input(">_").lower()
        if var == 'y':
            return
        elif var == 'n':
            raise UserAbortException


def user_input_range(range_list: range, message=None):
    """
    Manages user input with a given range

    :param range_list: the range to let user user chose from
    :type range_list: range
    :param message: a custom msg that can be displayed if the choice is invalid, defaults to None
    :param message: str, optional
    """    
    result = None

    while not result:
        var = input(">_").lower().replace(" ", "").split(',')
        result = var

        for v in var:
            try:
                iv = int(v)
            except ValueError:
                print(message) if message else None
                result = None
                break

            if iv not in range_list:
                print(message) if message else None
                result = None
                break

    return result


class UserAbortException(Exception):
    """
    Custom Exception raised if the user chosed to abort 
    """
    pass
