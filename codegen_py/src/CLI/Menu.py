from src.Util.Core import *
from colorama import Fore, Back


class Menu:
    """
    Simple Cli Menu to Build a User Interface
    every menu item can host n different submenus
    """

    __prompt = ">_"
    __selector_back = '0'

    def __init__(self, selecctor, name, f=None, args: list=None, sub_menus=None):
        """
        :param selecctor: the selector the user must enter to execute the menu
        :type selecctor: str
        :param name: display name of the menu
        :type name: str
        :param f: function to execute when menu is chosen, defaults to None
        :param f: function, optional
        :param args: arguments to pass to param f, defaults to None
        :param args: list, optional
        :param sub_menus: submenus the menu holds, defaults to None
        :param sub_menus: class Menu, optional
        """

        self.selector = selecctor
        self.name = name
        self.f = f
        self.parent = None
        self.sub_menus = []
        self.args = args

        if sub_menus:
            for sub in sub_menus:
                self.__add_submenu(sub)

    @staticmethod
    def clear():
        """
        simple screen clear method
        """
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")
        print("\n")

    def run_menu(self):
        """
        main function to execute a menu and show subemnus
        waits for user input -> Blocking call
        """
        print(Back.CYAN + Fore.BLACK + "        " + self.name + "        ")
        # execute the stored function
        if self.f:
            self.f() if self.args is None else self.f(*self.args)

        # show the back selection if we have a parent
        if self.parent:
            print("(" + self.__selector_back + ") ", "Back")

        # show all possible selections
        for sub in self.sub_menus:
            print("(" + sub.selector + ") ", sub.name)

        # wait for the users choice and execute it
        choice = input(self.__prompt)
        self.__exec_menu(choice)

    def __add_submenu(self, sub):
        """
        adds a submenu to the sub_menus property
        
        :param sub: the submenu to add
        :type sub: class Menu
        """
        assert isinstance(sub, Menu)
        if sub.selector == self.__selector_back:
            raise Exception("Selector %s is reserved" % self.__selector_back)
        if self.sub_menus:
            s = find(lambda item: item.selector == sub.selector, self.sub_menus)
            if s:
                raise Exception("Can not add submenu with same selector twice")
        self.sub_menus.append(sub)

    def __back(self):
        """
        Go back to parent menu with this method
        """
        if self.parent:
            if self.parent.sub_menus:
                self.parent.run_menu()

    def __exec_menu(self, choice):
        """
         execute a new user choice within the menu tree

        :param choice: the user choice 
        :type choice: str
        """
        self.clear()
        ch = choice.lower()
        if ch == self.__selector_back:
            self.__back()

        sub = find(lambda item: item.selector == ch, self.sub_menus)
        if sub:
            sub.parent = self
            sub.run_menu()
        else:
            print(Fore.YELLOW + "Invalid selection!")
            self.run_menu()
