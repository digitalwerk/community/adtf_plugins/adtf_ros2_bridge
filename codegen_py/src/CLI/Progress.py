class PBar:
    """
    Simple text based pbar
    """
    def __init__(self, max_steps, size=100, msg=""):
        """   
        :param max_steps: the number of steps required to reach 100%
        :type max_steps: int
        :param size: [description], defaults to 100
        :param size: int, optional
        :param msg: message to display above progressbar, defaults to ""
        :param msg: str, optional
        """
        self.max_steps = max_steps
        self.p = self.__start_progress()()  # () to get the iterator from the generator
        self.header_printed = False
        self.msg = msg
        self.size = size

    def step(self, reset=False):
        """
        Use this method to display the next step
        :param reset: reinit the bar
        :return:
        """
        if reset:
            self.__init__(self.max_steps, self.size, self.msg)
        if not self.header_printed:
            self.__print_header()

        try:
            next(self.p)
        except StopIteration:
            pass

    def max(self):
        """
        This method lets the pbar run to Max
        """
        while True:
            try:
                next(self.p)
            except StopIteration:
                break
        print()

    def __print_header(self):
        print()
        format_string = "0%{: ^" + str(self.size - 6) + "}100%"
        print(format_string.format(self.msg))
        self.header_printed = True

    def __start_progress(self):
        def print_progress():
            # Bresenham's algorithm. Yields the number of dots printed.
            dx = self.max_steps
            dy = self.size
            d = dy - dx
            for x in range(self.max_steps):
                k = 0
                while d >= 0:
                    print('=', end="", flush=True)
                    k += 1
                    d -= dx
                d += dy
                yield k

        return print_progress
