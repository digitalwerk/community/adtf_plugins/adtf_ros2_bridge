from src.Util.Core import *

ROS_COMMENT_INDICATOR = '#'
ROS_DOT_MSG_FILE = '.msg'
ROS_MSG_FILE = "msg"
ROS_ARRAY_UNSUPPORTED = ["[]", "[<="]
DDL_ARRAY_UNSUPPORTED = [-1]


class ROS2(ValueEnum):
    SRC = "src" + os.sep + "ros2" + os.sep
    INSTALL = "install"
    SHARE = "share"
    INSTALL_SHARE = "install" + os.sep + "share"
    INSTALL_INCLUDE = "share" + os.sep + "include"
    LOCAL_SETUP_WIN = "install" + os.sep + "local_setup.bat"
    LOCAL_SETUP_UNX = "install" + os.sep + "local_setup.bash"
    BUILD = "build"


class DDL_TAGS(ValueEnum):
    STRUCTS = "structs"
    STRUCT = "struct"
    ELEMENT = "element"
    TYPE = "type"
    NAME = "name"
    ARRAYSIZE = "arraysize"
    VERSION = "version"
    ALIGNMENT = "alignment"
    DESERIALIZED = "deserialized"
    SERIALIZED = "serialized"
    BYTEPOS = "bytepos"
    BYTEORDER = "byteorder"
    LITTLE_ENDIAN = "LE"


class JSON_TAGS(ValueEnum):
    MSG_FILES = "msg_files"
    MSG_NAME = "msg_name"
    MSG_HEADER = "msg_header"
    MSG_PATH_LIST = "msg_path_list"
    MSG_DEPENDENCIES = "msg_dependencies"
    MSG_XML_STRING = "msg_xml_string"
    MSG_XML_STRING_CLEAN = "msg_xml_string_clean"
    MSG_DATA_PATHS = "msg_data_paths"
    MSG_BUILD_DEPENDENCIES = "msg_build_dependencies"
    MSG_EXEC_DEPENDENCIES = "msg_exec_dependencies"
    PKG_NAME = "pkg_name"
    PKG_VERSION = "pkg_version"
    DDL_STRUCTS = "ddl_structs"
    ADTF_SRC_PATH = "adtf_path"
    ADTF_PLUGIN_INSTALL_PATH = "plugin_path"
    IS_KNOWN = "is_known"
    KNOWN_PATH = "known_path"
    KNOWN_PKG = "known_pkg"
    KNOWN_MSG = "known_msg"
    DEPENDENCIES = "dependencies"
    ADTF_DATA_PATH = "adtf_data_path"
    ROS_DATA_PATH = "ros_data_path"
    DATA_TYPE = "data_type"
    STREAM_NAME = "stream_name"
