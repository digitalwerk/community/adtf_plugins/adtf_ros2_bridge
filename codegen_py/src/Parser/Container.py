from abc import ABC, abstractmethod
from src.Parser.Helper import *


class MsgElementBase(ABC):
    """
    Abstract Msg Element Base class
    Simple Container class interface
    wiht abstract Constructor class cant be intantiated
    """
    @abstractmethod
    def __init__(self, name, data_type, byte_length, array_size=1, xml_element=None):
        self.name = name
        self.data_type = data_type
        self.array_size = array_size
        self.byte_length = byte_length * self.array_size
        self.xml_element = xml_element


class MsgElement(MsgElementBase):
    """
    MsgElement container for DDL Element
    needed for parsing in both directions
    the data paths in this class are used
    when parsing from msg to ddl
    """
    def __init__(self, name, data_type, byte_length, array_size=1, xml_element=None):
        """     
        :param name: name of the element 
        :type name: str
        :param data_type: data_type of element primitve or struct
        :type data_type: str
        :param byte_length: length of element in bytes
        :type byte_length: int
        :param array_size: size of array if 1 no array, defaults to 1
        :param array_size: int, optional
        :param xml_element: correspinding xml tree element, defaults to None
        :param xml_element: xml tree element, optional
        """
        super().__init__(name, data_type, byte_length, array_size, xml_element)
        self.adtf_data_path = None
        self.ros_data_path = None
        self.adtf_data_path_as_list = []
        self.ros_data_path_as_list = []

    def add_data_paths(self, adtf_data_path, ros_data_path):
        """
        add a data path to the element

        :param adtf_data_path: adtf, ddl path to data
        :type adtf_data_path: dict
        :param ros_data_path: ros. msg path to data
        :type ros_data_path: dict
        """
        self.adtf_data_path = adtf_data_path.copy()
        self.ros_data_path = ros_data_path.copy()

    def make_data_path_list(self):
        """
        Helper Method to convert the dicts
        to a list containing all the
        seperated data paths
        """
        init = False
        tmp_list_1 = []
        tmp_list_2 = []

        assert len(self.adtf_data_path) == len(self.ros_data_path)
        for (key1, val1), (key2, val2) in zip(self.adtf_data_path.items(), self.ros_data_path.items()):
            assert val1 == val2
            if val1 > 1:
                if not init:
                    for i in range(val1):
                        tmp_list_1.append(key1 + "[%s]" % str(i))
                        tmp_list_2.append(key2 + ".at(%s)" % str(i))
                    init = True
                else:
                    i = 0
                    for item1, item2 in zip(tmp_list_1, tmp_list_2):
                        item1 += '.' + key1 + "[%s]" % str(i)
                        item2 += '.' + key2 + ".at(%s)" % str(i)
                        i += 1
            else:
                if not init:
                    tmp_list_1.append(key1)
                    tmp_list_2.append(key2)
                    init = True
                else:
                    for item1, item2 in zip(tmp_list_1, tmp_list_2):
                        item1 += '.' + key1
                        item2 += '.' + key2

            self.adtf_data_path_as_list = tmp_list_1.copy()
            self.ros_data_path_as_list = tmp_list_2.copy()


class MsgStruct(MsgElementBase):
    """
    MsgStruct container for DDL Struct
    needed to calculate the bytepos offsets
    """
    def __init__(self, name, data_type, array_size=1, xml_element=None):
        """     
        :param name: name of the element 
        :type name: str
        :param data_type: data_type of element primitve or struct
        :type data_type: str
        :param byte_length: length of element in bytes
        :type byte_length: int
        :param array_size: size of array if 1 no array, defaults to 1
        :param array_size: int, optional
        :param xml_element: correspinding xml tree element, defaults to None
        :param xml_element: xml tree element, optional
        """
        super().__init__(name, data_type, 0, array_size, xml_element)

    def calculate_byte_length_callback(self, struct):
        """
        calculates the byte length of the struct


        :param struct: the parent struct container containing all the elements of this struct 
        :type struct: class MsgParentStruct
        """
        assert isinstance(struct, MsgParentStruct)
        self.byte_length = struct.byte_length * self.array_size


class MsgParentStruct:
    """
    Container class for parent structs
    needed for parsing in both directions
    the data paths in this class are used
    when parsing from ddl to msg
    """
    def __init__(self, name, original_name=None):
        """
        :param name: name of the struct
        :type name: str
        :param original_name: orignal name of the struct parsed from the ddl defaults to None
        :param original_name: str, optional
        """
        self.name = name
        self.original_name = original_name
        self.elements = []
        self.byte_length = 0
        self.known_path = None
        self.known_pkg = None
        self.known_msg = None
        self.is_known = False
        self.dependencies = []
        self.adtf_data_path = {}
        self.data_paths_as_list = []
        self.xml_string = None

    def add_data_path(self, **adtf_data_path):
        """
        add a new data path
        
        """
        self.adtf_data_path.update(adtf_data_path)

    def add_dependencie(self, other_struct):
        """
        adds another struct to the dependencies of this struct
        
        :param other_struct: the struct which is neeeded
        :type other_struct: class MsgParentStruct
        """
        assert isinstance(other_struct, MsgParentStruct)
        self.dependencies.append(other_struct.name)

    def make_known(self, path_to_ros_msg, ros_pkg, msg):
        """
        if the struct was found inside the ros installation 
        this method is executed to make it know for further processing 
        
        :param path_to_ros_msg: the abs. path to the ros msg file representing this struct
        :type path_to_ros_msg: str, pathlike
        :param ros_pkg: name of the ros package where the msg representing this struct lives in 
        :type ros_pkg: str
        :param msg: name of the msg file representing this struct
        :type msg: str
        """
        self.known_path = path_to_ros_msg
        self.known_pkg = ros_pkg
        self.known_msg = msg
        self.is_known = True

    def add_element(self, element):
        """
        Add a elment as child to this struct
       
        :param element: the element to add
        :type element: class MsgElementBase
        """
        assert isinstance(element, MsgElementBase)
        self.elements.append(element)
        self.byte_length += element.byte_length

    def get_info(self):
        """
        Get all the info to this struct as json like dict
        :return: a dictionary in which contains several sub_dicts
        :rtype: dict
        """
        return {self.name: {
            JSON_TAGS.STREAM_NAME: self.original_name,
            JSON_TAGS.IS_KNOWN: self.is_known,
            JSON_TAGS.KNOWN_PATH: self.known_path,
            JSON_TAGS.KNOWN_PKG: self.known_pkg,
            JSON_TAGS.KNOWN_MSG: self.known_msg,
            JSON_TAGS.DEPENDENCIES: self.dependencies,
            JSON_TAGS.MSG_DATA_PATHS: self.data_paths_as_list,
            JSON_TAGS.MSG_XML_STRING: self.xml_string,
            JSON_TAGS.MSG_XML_STRING_CLEAN: xml_to_cString(self.xml_string)}
        }

    def get_xml(self):
        """
        :return: a xml element representing this struct 
        :rtype: class Et.Element
        """

        xml = Et.Element(
            DDL_TAGS.STRUCT,
            {DDL_TAGS.NAME: self.original_name,
             DDL_TAGS.VERSION: str(1),
             DDL_TAGS.ALIGNMENT: str(1)}
        )

        for element in self.elements:
            xml.append(element.xml_element)

        return xml
