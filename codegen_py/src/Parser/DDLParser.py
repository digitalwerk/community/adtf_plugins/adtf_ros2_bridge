from src.Parser.Container import *
from src.Parser.Helper import *
from src.Parser.Constants import *
from colorama import Fore
from src.Util.Temp import *
from src.CLI.Progress import *


class DDL_Parser:
    """
    ADTF_DDL_Parser class
    with given abs. path to *.xml or txt file and export path as well as the ros src dir
    this object parses all the information in ros2 msg files and a list of dependencies
    as json ot pkl
    """
    PBAR_MAX = 15

    # Enum representing the supported Filetypes
    class SupportedImportTypes(Enum):
        txt = "txt"
        xml = "xml"

    class SupportedExportTypes(Enum):
        json = "json"
        pickle = "pkl"

    # Constructor # execution
    def __init__(self, ros_src_path, ddl_file, export_path, current_pkg, **exports):
        """
        :param ros_src_path: path to the ros src of the ros installation
        :type ros_src_path: str, pathlike
        :param ddl_file: path to the file to parse
        :type ddl_file: str, pathlike
        :param export_path: path to export dir
        :type export_path:  str, pathlike
        :param current_pkg: currently used message package, needed to get dependencies right
        :type current_pkg: str
        """
        self.pBar = PBar(self.PBAR_MAX, msg="Parsing " + ddl_file.split(os.sep)[-1])
        self.pBar.step()
        validate_path(ros_src_path)
        self.pBar.step()
        validate_path(ddl_file)
        self.pBar.step()
        validate_path(export_path)

        list_enum = [x.value for x in self.SupportedExportTypes]
        for file_ending in exports.keys():
            if file_ending not in list_enum:
                raise Exception(
                    "The filetype '%s' is not supported for Import only '%s'" 
                    % (file_ending, ", ".join(list_enum))
                )

        self.ros_src_path = ros_src_path
        self.ddl_file = ddl_file
        self.xml_raw_tree = None
        self.export_path = export_path
        self.struct_list = []
        self.ignored_tags = []
        self.current_pkg = current_pkg
        self.exports = exports.copy()
        self.file_type = self._check_file_ending(self.ddl_file)
        self.pBar.step()

        if self.file_type == DDL_Parser.SupportedImportTypes.xml:
            self.__parse_xml_file()
        elif self.file_type == DDL_Parser.SupportedImportTypes.txt:
            self.__parse_txt_file()

        self.pBar.step()
        self.__check_for_known_structs()
        self.pBar.step()
        self.__check_dependencies()
        self.pBar.step()
        self.__concat_xml_structs()
        self.pBar.step()
        self.__get_data_paths()
        self.pBar.step()
        self.__make_files()
        self.pBar.max()

        print(Fore.YELLOW + "The XML tags %s were ignored"
              % list_to_pretty_string(list(set(self.ignored_tags))))

    @staticmethod
    def _check_file_ending(file_path):
        """
        strips the given file_path down to the fileending and checks
        whether the type is supported or not
        
        :param file_path: the absolute filepath
        :type file_path: str, pathlike
        :return: SupportedFileTypes.x the filetype as enum element
        :rtype: Enum
        :exception FileTypeNotSupportedException raised if fileending is not supported
        """
        file = file_path.split(os.sep)[-1].split(os.extsep)[-1]
        for filetype in DDL_Parser.SupportedImportTypes:
            if file == filetype.name:
                return filetype
        raise FileTypeNotSupportedException()

    def __parse_txt_file(self):
        """
        Used to parse txt files. 
        Generates a tmp file if needed
        """
        # cheack if file is cString like
        if file_is_cString(self.ddl_file):

            # when the file is written like a cString
            # we just convert it line by line and write it
            # intop a simple tempfile which is deleted after use
            with open(self.ddl_file, "r") as f:
                lines = f.readlines()

            tf, path = TempFileDistributor.get_file()
            with os.fdopen(tf, "w") as f:
                for line in lines:
                    f.write(cString_to_xml(line))

            self.ddl_file = path
            self.__parse_xml_file()
        else:
            self.__parse_xml_file()

    def __parse_xml_file(self):
        """
        starts the parsing of the xml tree
        with the tree root node
        """
        self.xml_raw_tree = Et.parse(self.ddl_file)
        self.__parse_xml_element(self.xml_raw_tree.getroot(), None)

    def __parse_xml_element(self, xml_root, parent_struct):
        """
        recursivly parses the xml tree into al lsit of structs with element childs

        :param xml_root: the root node of the xml tree to parse
        :type xml_root: xml Element/Tree
        :param parent_struct: parent struct object to append to
        :type parent_struct: class MsgParentStruct or None if not in recursion
        """

        for child in xml_root:
            if child.tag == DDL_TAGS.STRUCTS:
                self.__parse_xml_element(child, None)

            elif child.tag == DDL_TAGS.STRUCT:
                msg_parent_struct = MsgParentStruct(
                    convert_msg_file_name(child.attrib[DDL_TAGS.NAME]),
                    child.attrib[DDL_TAGS.NAME]
                )

                self.struct_list.append(msg_parent_struct)
                self.__parse_xml_element(child, msg_parent_struct)

            elif child.tag == DDL_TAGS.ELEMENT:
                assert isinstance(parent_struct, MsgParentStruct or None)
                data_type = child.attrib[DDL_TAGS.TYPE]
                element_name = child.attrib[DDL_TAGS.NAME]
                array_size = int(child.attrib[DDL_TAGS.ARRAYSIZE])

                # check the arraysize
                if is_array_invalid(array_size=array_size):
                    raise Exception(
                        "Array is invalid %s inside of %s" % (array_size, child.tag))

                if is_struct(data_type):
                    name = convert_msg_variable_name(element_name)
                    msg_element = MsgStruct(
                        name,
                        convert_msg_file_name(data_type),
                        array_size,
                        child
                    )
                else:
                    supported_data_type = SupportedDataTypes.get_ros_data_type(data_type)
                    name = convert_msg_variable_name(element_name)
                    msg_element = MsgElement(
                        name,
                        supported_data_type.ros_data_type,
                        supported_data_type.byte_length,
                        array_size,
                        child
                    )
                # add the element to the struct
                parent_struct.add_element(msg_element)
                # and the corresponding adtf data path
                parent_struct.add_data_path(**{element_name: [array_size, data_type, name]})

            else:
                self.ignored_tags.append(child.tag)

    def __check_for_known_structs(self):
        """
        Iterates over struct list and checks if structs are already known
        or if the struct needs to be generated information is added to the container

        :raises MsgAmbiguousException: if the msg file is found to often
        """

        for struct in self.struct_list:
            assert isinstance(struct, MsgParentStruct)
            msg_files_list = find_files(self.ros_src_path, struct.name + ROS_DOT_MSG_FILE)

            if len(msg_files_list) == 2:
                self.pBar.step()
                self.__check_known_struct_content(msg_files_list[0], struct)

                struct.make_known(msg_files_list[0],
                                  get_pkg_from_xml(msg_files_list[0]),
                                  get_file_from_path(msg_files_list[0]))
            elif len(msg_files_list) > 2:
                raise MsgAmbiguousException("The file: %s is ambiguous" % str(msg_files_list))

    @staticmethod
    def __check_known_struct_content(msg_file, struct):
        """
        checks if the content of given msg file matches the struct
        
        :param msg_file: msg file to check
        :type msg_file: str, pathlike
        :param struct: struct corresponing to msg_file
        :type struct: class MsgParentStruct
        :raises FileNotFoundError: if the given msg file is not found
        :raises Exception: if content of msg file does not match the contetn of the struct
        """
        assert isinstance(struct, MsgParentStruct)
        try:
            path_to_file = Path(msg_file)

            with path_to_file.open() as file:
                content = file.readlines()
            content = [x.strip() for x in content]

            # first cheack the lenght of both lists
            if len(struct.elements) != len(content):
                raise Exception(
                    "Mismatch parsed Struct %s does not match the content of %s" % (struct.name, msg_file) +
                    os.linesep + "The Element count does not match %s != %s" % (str(len(struct.elements)), str(len(content)))
                )

            # check the individual element
            for (element, line) in zip(struct.elements, content):
                data_type, name = line.split()
                array_size = get_array_size(data_type)
                data_type_stripped = strip_type_name(data_type)

                if is_array_invalid(unstripped_type_name=data_type):
                    raise Exception(
                        "Array is invalid %s inside of %s" % (data_type, msg_file))

                if name != element.name or data_type_stripped != element.data_type:
                    raise Exception(
                        "Mismatch parsed struct %s does not match the content of %s" % (struct.name, msg_file) +
                        os.linesep + "'(%s, %s) != (%s, %s)'" % (element.name, element.data_type, name, data_type_stripped)
                    )

                if element.array_size != get_array_size(data_type):
                    raise Exception(
                        "Mismatch parsed struct %s does not match the content of %s" % (struct.name, msg_file) +
                        os.linesep + " Arraysize [%s] != Arraysize [%s]" % (str(element.array_size), str(array_size))
                    )

        except FileNotFoundError:
            raise

    def __check_dependencies(self):
        """
        Links all the dependencies between the single struct elements

        :raises DependencieException: if any broken dependencies are found       
        """

        for struct in self.struct_list:
            assert isinstance(struct, MsgParentStruct)

            for element in struct.elements:
                if isinstance(element, MsgStruct):
                    match = find(lambda item: item.name == element.data_type, self.struct_list)
                    if match:
                        struct.add_dependencie(match)
                    else:
                        raise DependencieException(
                            "Broken dependencie link inside the parsed file %s.\n"
                            "Could not match %s to a known msg file or a struct inside the file."
                            % (self.ddl_file, element.data_type)
                        )

    def __concat_xml_structs(self):
        """
        Concats all xml elements as string with respect to their dependencies

        :raises DependencieException: if any broken dependencies are found 
        """
        for struct in self.struct_list:
            assert isinstance(struct, MsgParentStruct)
            xml = Et.Element(DDL_TAGS.STRUCTS)

            for name in struct.dependencies:
                match = find(lambda item: item.name == name, self.struct_list)
                if match:
                    x = minidom.parseString(Et.tostring(match.get_xml())).toprettyxml(indent="", newl="")
                    xml.append(Et.fromstring(touglyxml(x)))
                else:
                    raise DependencieException(
                        "Broken dependencie link inside the parsed file %s.\n"
                        "Could not match %s to a known msg file or a struct inside the file."
                        % (self.ddl_file, name)
                    )

            y = minidom.parseString(Et.tostring(struct.get_xml())).toprettyxml(indent="", newl="")
            xml.append(Et.fromstring(touglyxml(y)))
            struct.xml_string = minidom.parseString(Et.tostring(xml)).toprettyxml(indent="\t", newl="\n")

    def __get_data_paths(self):
        """
        generates the data paths for all structs in the list
        
        """
        for struct in self.struct_list:
            struct.data_paths_as_list = self.__generate_data_paths(struct)

    def __generate_data_paths(self, struct):
        """
        this method generates the additional information needed to generate the code
        for each struct all alements are broken down to simple primitive datatypes and the
        paths to this types are added to a dict recursivly
        
        :param struct: MsgParentStruct object to process
        :type struct: class MsgParentStruct     
        :return: a list of dictionarys with information about datatype and adtf and ros data paths
        :rtype: list
        :raises DependencieException: if any broken dependencies are found 
        """
        result = []
        assert isinstance(struct, MsgParentStruct)

        for key, value in struct.adtf_data_path.items():
            # is array
            if value[0] > 1:
                # is struct, recursivly add paths of struct if found else raise exception
                if is_struct(value[1]):
                    found_struct = find(lambda item: item.name == convert_msg_file_name(value[1]), self.struct_list)
                    if found_struct:
                        tmp_result = self.__generate_data_paths(found_struct)
                        # append in range of array size
                        for i in range(value[0]):
                            for res in tmp_result:
                                result.append({JSON_TAGS.DATA_TYPE: res[JSON_TAGS.DATA_TYPE],
                                               JSON_TAGS.ROS_DATA_PATH: value[2] + ".at(%i)." % i + res[JSON_TAGS.ROS_DATA_PATH],
                                               JSON_TAGS.ADTF_DATA_PATH: key + "[%i]" % i + res[JSON_TAGS.ADTF_DATA_PATH]})

                    else:
                        raise DependencieException("Broken dependencie link inside the parsed file %s.\n"
                                                   "Could not match %s to a known msg file or a struct inside the file."
                                                   % (self.ddl_file, value[1]))
                # no struct
                else:
                    # append in range of array size
                    for i in range(value[0]):
                        result.append({JSON_TAGS.DATA_TYPE: value[1],
                                       JSON_TAGS.ROS_DATA_PATH: value[2] + ".at(%i)" % i,
                                       JSON_TAGS.ADTF_DATA_PATH: key + "[%i]" % i})

            # no array
            else:
                # is struct, recursivly add paths of struct if found else raise exception
                if is_struct(value[1]):
                    found_struct = find(lambda item: item.name == convert_msg_file_name(value[1]), self.struct_list)
                    if found_struct:
                        tmp_result = self.__generate_data_paths(found_struct)
                        # append in range of array size
                        for i in range(value[0]):
                            for res in tmp_result:
                                result.append({JSON_TAGS.DATA_TYPE: res[JSON_TAGS.DATA_TYPE],
                                               JSON_TAGS.ROS_DATA_PATH: value[2] + "." + res[JSON_TAGS.ROS_DATA_PATH],
                                               JSON_TAGS.ADTF_DATA_PATH: key + "." + res[JSON_TAGS.ADTF_DATA_PATH]})
                    else:
                        raise DependencieException("Broken dependencie link inside the parsed file %s.\n"
                                                   "Could not match %s to a known msg file or a struct inside the file."
                                                   % (self.ddl_file, value[1]))
                else:
                    # no struct simple
                    result.append({JSON_TAGS.DATA_TYPE: value[1],
                                   JSON_TAGS.ROS_DATA_PATH: value[2],
                                   JSON_TAGS.ADTF_DATA_PATH: key})

        return result

    def __make_files(self):
        """
        generates the needed msg files and
        their dependency exports in the export dir
        
        :raises FileTypeNotSupportedException: when the given filetype is not supported
        """
        dependencies = {}

        for struct in self.struct_list:
            assert isinstance(struct, MsgParentStruct)

            if not struct.is_known:
                msg_file = os.path.join(self.export_path, struct.name + ROS_DOT_MSG_FILE)
                msg_file_list = [self.__make_msg_file_line(element) for element in struct.elements]
                make_file(msg_file, "\n", *msg_file_list)

            dependencies.update(struct.get_info())

        for key, value in self.exports.items():
            if key == self.SupportedExportTypes.json.value:
                filename = os.path.join(self.export_path, value)
                make_json_file(filename, **{JSON_TAGS.DDL_STRUCTS: dependencies})

            elif key == self.SupportedExportTypes.pickle.value:
                filename = os.path.join(self.export_path, value)
                make_pickle_file(filename, **{JSON_TAGS.DDL_STRUCTS: dependencies})
            else:
                raise FileTypeNotSupportedException()

    def __make_msg_file_line(self, element):
        """
        with given MsgElement this helper method creates a string representing the element as line of a ros2 msg file

        :param element: element to convert
        :type element: MsgElement, MsgStruct
        :return: line in msg file
        :rtype: str
        :raises DependencieException: if any broken dependencies are found 
        """
        if isinstance(element, MsgElement):
            if element.array_size > 1:
                return str(element.data_type + "[%i]" % element.array_size + " " + element.name)
            else:
                return str(element.data_type + " " + element.name)

        if isinstance(element, MsgStruct):
            pkg = self.current_pkg
            found_struct = find(lambda item: item.name == element.data_type, self.struct_list)
            if found_struct:
                struct_info = found_struct.get_info()
                is_known = struct_info[found_struct.name][JSON_TAGS.IS_KNOWN]
                known_pkg = struct_info[found_struct.name][JSON_TAGS.KNOWN_PKG]
                if is_known and known_pkg != self.current_pkg:
                    pkg = known_pkg
            else:
                raise DependencieException("Broken dependencie link inside the parsed file %s.\n"
                                           "Could not match %s to a struct."
                                           % (self.ddl_file, element.data_type))
            if pkg != self.current_pkg:
                if element.array_size > 1:
                    return str(pkg + os.altsep + element.data_type + "[%i]" % element.array_size + " " + element.name)
                else:
                    return str(pkg + os.altsep + element.data_type + " " + element.name)
            else:
                if element.array_size > 1:
                    return str(element.data_type + "[%i]" % element.array_size + " " + element.name)
                else:
                    return str(element.data_type + " " + element.name)
