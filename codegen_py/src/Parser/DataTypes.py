from src.Parser.Exception import *
from src.Parser.Constants import *
import os


def is_array_invalid(unstripped_type_name=None, array_size=None):
    """
    checks if array is invalid use only named params for this function
    !!!set only one of the two params!!!
    
    :param unstripped_type_name: name of datatype, defaults to None
    :type unstripped_type_name: str, optional
    :param array_size: array_size from ddl, defaults to None
    :type array_size: int, optional
    """
    assert unstripped_type_name is None or array_size is None, "One must be None"

    if unstripped_type_name:
        for x in ROS_ARRAY_UNSUPPORTED:
            if x in unstripped_type_name:
                return True
        return False

    elif array_size:
        for x in DDL_ARRAY_UNSUPPORTED:
            if x == array_size:
                return True
        return False


def get_array_size(unstripped_type_name):
    """
    if a type is array the size is returned else 1

    :param unstripped_type_name: name of datatype
    :type unstripped_type_name: str
    :return: size of array, >1 if array else 1
    :rtype: int
    :raises NotImplementedError: if type_name contains ARRAY_UNSUPPORTED
    """
    if "[" in unstripped_type_name:
        if is_array_invalid(unstripped_type_name=unstripped_type_name):
            raise NotImplementedError("Unsupported array type, a specific length must be given")
        num_list = [int(s) for s in unstripped_type_name.replace("[", " ").replace("]", " ").split() if s.isdigit()]
        return num_list[0]
    else:
        return 1


def is_struct(stripped_type_name):
    """
    checks if the given typename is a struct or basic datatype

    :param stripped_type_name: name of datatype
    :type stripped_type_name: str
    :return: False if type is primitive else True
    :rtype: bool
    """
    for datatype in SupportedDataTypes:
        if stripped_type_name == datatype.ros_data_type \
                or stripped_type_name == datatype.adtf_data_type:
            return False
    return True


def is_const(element_name):
    """
    based on the typename this helper checks if the element_name represents a const value

    :param element_name:
    :type element_name: str
    :return: True / False
    :rtype: bool
    """
    if element_name.isupper() and '=' in element_name:
        return True
    return False


def strip_type_name(unstripped_type_name):
    """
    removes array decorator

    :param unstripped_type_name: name of datatype
    :type unstripped_type_name: str
    :return: datatype without array decorator
    :rtype: str 
    """
    stripped_type_name = [s for s in unstripped_type_name.replace("[", "").replace("]", "").split()][0]
    if '/' in stripped_type_name:
        return stripped_type_name.split(os.altsep)[-1]
    return stripped_type_name


class DataType(Enum):
    """
    Enum type class for supportet datatypes
    getter Methods for property return values
    """
    def __new__(cls, ros_data_type, adtf_data_type, byte_length):
        obj = object.__new__(cls)
        obj._byte_length = byte_length
        obj._ros_data_type = ros_data_type
        obj._adtf_data_type = adtf_data_type
        return obj

    @property
    def adtf_data_type(self):
        return self._adtf_data_type

    @property
    def ros_data_type(self):
        return self._ros_data_type

    @property
    def byte_length(self):
        return self._byte_length


class SupportedDataTypes(DataType):
    """
    Enum collection of supported datatypers with
    ros_type_name, adtf_type_name and bytelength
    """
    bool = ("bool", "tBool", 1)
    uint8 = ("uint8", "tUInt8", 1)
    # since byte is only a ros known datatype
    # we place it in the enum list behind uint8
    # this way it will return uint8 if a lookup is made
    # via get_ros_data_type("tUInt8")
    # in the opposite direction
    # get_adtf_data_type("byte") will return "tUInt8"
    byte = ("byte", "tUInt8", 1)
    char = ("char", "tChar", 1)
    float32 = ("float32", "tFloat32", 4)
    float64 = ("float64", "tFloat64", 8)
    int8 = ("int8", "tInt8", 1)
    int16 = ("int16", "tInt16", 2)
    uint16 = ("uint16", "tUInt16", 2)
    int32 = ("int32", "tInt32", 4)
    uint32 = ("uint32", "tUInt32", 4)
    int64 = ("int64", "tInt64", 8)
    uint64 = ("uint64", "tUInt64", 8)

    @staticmethod
    def get_adtf_data_type(ros_data_type):
        """
        lookup for the adtf_data_type

        :param ros_data_type: name of the ros_data_type
        :type ros_data_type: str
        :raises DataTypeNotSupportedException: raised if the param ros_data_type could not be found
        :return: the corresponding adtf_data_type
        :rtype: str
        """

        for datatype in SupportedDataTypes:
            if ros_data_type == datatype.ros_data_type:
                return datatype
        raise DataTypeNotSupportedException("The Datatype: %s is not supported or is invalid" % ros_data_type)

    @staticmethod
    def get_ros_data_type(adtf_data_type):
        """
        lookup for the ros_data_type

        :param adtf_data_type: name of the adtf_data_type
        :type adtf_data_type: str
        :raises DataTypeNotSupportedException: raised if the param adtf_data_type could not be found
        :return: the corresponding ros_data_type
        :rtype: str
        """
        for datatype in SupportedDataTypes:
            if adtf_data_type == datatype.adtf_data_type:
                return datatype
        raise DataTypeNotSupportedException(
            'The Datatype: %s is not supported or is invalid' % adtf_data_type)
