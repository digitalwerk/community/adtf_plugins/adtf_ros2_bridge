class MsgNotFoundException(Exception):
    """
    Just a simple custom exception
    used if a ros msg file could not be located
    inside the ros2 src dir
    """
    pass


class MsgNotParsableException(Exception):
    """
    Just a simple custom exception
    used if a ros msg file van not be parsed
    """
    pass


class MsgAmbiguousException(Exception):
    """
    Just a simple custom exception
    used more than one ros msg file
    with the same name was found
    inside the ros2 src dir
    """
    pass


class DataTypeNotSupportedException(Exception):
    """
    Just a simple custom exception
    used when a datatype in a ros2 msg
    was found which is not supported
    """
    pass


class FileTypeNotSupportedException(Exception):
    """
    Just a simple custom exception
    used when a filetype is used
    that is not supported
    """
    pass


class DependencieException(Exception):
    """
    Just a simple custom exception thrown if
    there is aa broken Dependencie while parsing
    """
    pass
