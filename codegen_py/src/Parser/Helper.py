from xml.dom import minidom
from src.Parser.DataTypes import *
from src.Parser.Constants import *
from pathlib import Path
import xml.etree.ElementTree as Et
import re

def file_is_cString(file):
    """
    Checks whether a the file content is likly to be encoded like a c-Style String
    
    :param file: file to check
    :type file: str, pathlike
    :return: True/False
    :rtype: bool
    """
    with open(file, "r") as f:
        data = f.readlines()

    return str_is_cString("".join(data))


def str_is_cString(s: str):
    """
    Checks whether a the stringt is likly to be encoded like a c-Style String

    :param s: string to check
    :type file: str
    :return: True/False
    :rtype: bool
    """
    data = s.split("\n")
    for line in data:
        if line.endswith('"') or line.startswith('"'):
            return True
    return False


def cString_to_xml(s: str):
    """
    Converts c-Style string to xml string
    
    :param s: string to convert
    :type s: str
    :return: converted string
    :rtype: str
    """
    return s.replace(">\"", ">").replace("\"<", "<").replace("\\\"", "\"")


def xml_to_cString(xml, start=1, end=None):
    """
    Converts c-Style string to xml string

    :param xml: the xml string to convert
    :type xml: str
    :param start: cutoff n lines counting from line 0, defaults to 1
    :param start: int, optional
    :param end: cutoff n lines counting from line max, defaults to None
    :param end: int, optional
    :return: string converted 
    :rtype: str
    """
    e = len(xml.split("\n")) if end is None else end
    return "\n".join([x.replace("\"", "\\\"").replace("<", "\"<").replace(">", ">\"") for x in xml.split("\n")][start:e])


def touglyxml(input_xml):
    """
    Deletes all the unneeded things from a xml
    
    :param input_xml: the xml object to convert
    :return: converted xml type
    """
    dom = minidom.parseString(input_xml)
    output_xml = ''.join([line.strip() for line in dom.toxml().splitlines()])
    dom.unlink()
    return output_xml


def convert_msg_file_name(name):
    """
    with given str this functions converst the string
    according to the ros2 msg file name conventions
    ---> CamelCase <--- like this
    
    :param name:the name of the msg file to convert
    :type name: str
    :return: converted name 
    :rtype: str
    """
    result = str(name)

    # remove unwanted chars
    result = re.sub("__", "_", result)
    pattern = re.compile("[^a-zA-Z_]")
    matches = pattern.finditer(result)

    offset = 0
    for match in matches:
        left = result[:match.span()[0] - offset]
        right = result[match.span()[1] - offset:]
        result = left + right
        offset += 1

    if result[0].islower():
        result = result[0].upper() + result[1:]

    return result


def convert_msg_variable_name(name):
    """
    converst the given variable name conforming to the ros2 msg format
    Field names must be lowercase alphanumeric characters with underscores for separating words.
    They must start with an alphabetic character,
    they must not end with an underscore and never have two consecutive underscores.

    :param name: name to convert
    :type name: str
    :return converted name
    :rtype: str
    """
    result = str(name)

    # delete leading numbers
    first_char = result[0]
    if first_char.isdigit():
        result = convert_msg_variable_name(result[1:])

    # delete leading underscores
    if result.startswith("_"):
        result = convert_msg_variable_name(result[1:])

    # delete trailing underscores
    if result.endswith("_"):
        result = convert_msg_variable_name(result[:-1])

    # make a speration with underscores
    for c in result:
        if c.isupper():
            idx = result.index(c)
            left = result[:idx]
            mid = result[idx].lower()
            right = result[idx + 1:]
            result = left + "_" + mid + right

    # remove double underscore
    while "__" in result:
        result = re.sub("__", "_", result)

    # remove unwanted chars
    pattern = re.compile("[^a-zA-Z0-9_]")
    matches = pattern.finditer(result)

    offset = 0
    for match in matches:
        left = result[:match.span()[0] - offset]
        right = result[match.span()[1] - offset:]
        result = left + right
        offset += 1

    return result


def get_hpp_header_from_msg_path(msg, lookup_dir):
    """
    with given msg path this functions searches for the corresponding hpp file in the ros filesystem.

    :param msg: the absolute filepath to the msg file
    :type msg: str, pathlike
    :param lookup_dir: the driectory where to look for the header, must be a part of param msg, defaults to ROS_PATH.INSTALL
    :param lookup_dir: str, optional
    :return: include path to header c style or None 
    :rtype: str, None
    """

    result = None
    msg_name = get_file_from_path(msg).replace(ROS_DOT_MSG_FILE, "").lower()
    msg_name_len = len(msg_name)
    pkg_name = get_pkg_from_xml(msg)

    all_hpp_files = find_files(lookup_dir, "*.hpp")

    if all_hpp_files:
        for hpp in all_hpp_files:
            hpp_name = get_file_from_path(hpp).replace(".hpp", "").replace("_", "").lower()

            if len(hpp_name) == msg_name_len:
                if hpp_name == msg_name:
                    if pkg_name in hpp:
                        hpp_list = hpp.split(os.sep)
                        idx = len(hpp_list) -1 - hpp_list[::-1].index(pkg_name)
                        result = hpp_list[idx:]
                        break

    return result


def get_path_list_from_msg_path(msg):
    """
    with given abs path to msg file this functions returns a list like:
    ["drive", "dir_1", ..., "dir_n", "*.msg"]

    :param msg: the msg to process
    :type msg: str, pathlike
    :return: the path to the file as list
    :rtype: list, None
    """
    msg_path_as_list = msg.split(os.sep)
    if len(msg_path_as_list) < 1:
        return None

    msg_path_as_list[-1] = msg_path_as_list[-1].replace(ROS_DOT_MSG_FILE, "")
    return msg_path_as_list


def get_all_msg_paths(ros_src_dir):
    """
    with given params this function returns a list of all ros2 msg files
    
    :param ros_src_dir: the source path to the ros2 installation
    :type ros_src_dir: str
    :param msg_dir: the subdir to look in, defaults to ROS_PATH.INSTALL_SHARE
    :type msg_dir: str, optional
    :return: list of all found filepaths
    :rtype: list
    """
    if os.name == 'nt':
        return find_files(os.path.join(ros_src_dir, ROS2.INSTALL_SHARE), "*" + ROS_DOT_MSG_FILE)
    else:
        return find_files(os.path.join(ros_src_dir, ROS2.INSTALL), "*" + ROS_DOT_MSG_FILE)


def get_pkg_from_path(msg_path, parent_dir=ROS2.SHARE):
    """
    [summary]
    
    :param msg_path: path to file
    :type msg_path: str, pathlike
    :param parent_dir: the dir parent to the pkg folders, defaults to ROS_PATH.SHARE
    :type parent_dir: str, optional
    :return: pkg name
    :rtype str:
    """
    dir_list = msg_path.split(os.sep)
    idx = dir_list.index(parent_dir) + 1
    return dir_list[idx] if idx < len(dir_list) else None


def get_pkg_from_xml(msg_path, abort_dir=ROS2.SHARE):
    """
    with given path to msg file this function
    searches the package.xml file and get the name tag from the xml
    this name tag represents the package name

    :param msg_path: path to msg file
    :type msg_path: str, pathlike
    :param abort_dir: dir to abort when reached, defaults to ROS_PATH.SHARE
    :type abort_dir: str, optional
    :return: package name or None
    :rtype: str, None
    """
    search_path = os.path.dirname(msg_path)
    while search_path.split(os.sep)[-1] != abort_dir:

        package_xml = find_files(search_path, "package.xml")
        if package_xml:
            xml_tag = search_xml_elements(Et.parse(package_xml[0]).getroot(), "name")
            if xml_tag:
                return xml_tag[0].text
        search_path = os.path.dirname(search_path)

    return None


def get_parsable_msgs(msg_paths: list):
    """
    with given list a new list is returned
    this list contains sublists like [filename. pkg name, abspath to file]
    alle items in the new list a valdidated

    :param msg_paths: list of all msg files
    :type msg_paths: list
    :return: a list of all msg file parsable 
    :rtype: list
    """
    result = []
    for msg in msg_paths:
        path_to_file = Path(msg)
        # read file content
        with path_to_file.open() as file:
            content = file.readlines()

            # validate the file content line by line
            invalid = False
            for line in content:
                line_content = line.split()
                if len(line_content) > 1:
                    # validate the array size
                    if is_array_invalid(unstripped_type_name=line_content[0]):
                        invalid = True
                        break
                    # if its not a struct cheack the data type
                    if not is_struct(strip_type_name(line_content[0])):
                        try:
                            SupportedDataTypes.get_adtf_data_type(strip_type_name(line_content[0]))
                        except DataTypeNotSupportedException:
                            invalid = True
                            break
                    else:
                        # if it is a struct check if its a comment
                        if not str(line_content[0]).startswith(ROS_COMMENT_INDICATOR):
                            if line_content[0] == "string":
                                invalid = True
                                break
                            else:
                                # check the coressponding message of the struct recursivly
                                match = find(lambda item: get_file_from_path(item) == line_content[0].split("/")[-1]+ROS_DOT_MSG_FILE, msg_paths)
                                if match:
                                    rec_result = get_parsable_msgs([match])
                                    if not rec_result:
                                        invalid = True
                                        break
                                else:
                                    invalid = True
                                    break    
            if not invalid:
                pkg = get_pkg_from_xml(msg)
                if pkg:
                    msg_file = get_file_from_path(msg)
                    result.append([msg_file, pkg, msg])
    return result


def search_xml_elements(xml_root, tag):
    """
    searches an xml tree recursivly for elements with ekement.tag == tag

    :param xml_root: the xml tree root
    :param tag: tag to look for
    :type tag: str
    :return: list with found matches or empty list
    :rtype: list
    """
    result = []
    for child in xml_root:

        if child.tag == tag:
            result.append(child)

        rec_result = search_xml_elements(child, tag)
        if rec_result:
            result.append(rec_result)

    return result
