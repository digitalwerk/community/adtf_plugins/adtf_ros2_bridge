from src.Parser.Container import *
from src.CLI.Progress import *


class MSG_Parser:
    """
    ROS_MSG_Parser class
    with given abs. path to *.msg file and export path as well as the ros src dir
    this object parses all the information in specified files like, json, xml, txt and pkl
    """
    PBAR_MAX = 12

    class SupportedImportTypes(Enum):
        msg = ROS_MSG_FILE

    class SupportedExportTypes(Enum):
        txt = "txt"
        xml = "xml"
        json = "json"
        pickle = "pkl"

    # Constructor # execution
    def __init__(self, ros_src_path, ros_msg_file, export_path, **exports):
            """           
            :param ros_src_path: path to ros src of ros installation
            :type ros_src_path: str, pathlike
            :param ros_msg_file: ros msg file to parse
            :type ros_msg_file: str, pathlike 
            :param export_path: file export dir
            :type export_path: str, pathlike 
            """
            self.pBar = PBar(self.PBAR_MAX, msg="Parsing " + ros_msg_file.split(os.sep)[-1])
            self.pBar.step()
            validate_path(ros_src_path)
            self.pBar.step()
            validate_path(ros_msg_file)
            self.pBar.step()
            validate_path(export_path)

            file_ending = ros_msg_file.split(os.extsep)[-1]
            list_enum = [x.value for x in self.SupportedImportTypes]
            if file_ending not in list_enum:
                raise Exception(
                    "The filetype '%s' is not supported for Import only '%s'"
                    % (ros_msg_file, ", ".join(list_enum))
                )

            self.export_path = export_path
            self.ros_msg_file = ros_msg_file
            self.ros_src_path = ros_src_path
            self.dependencies = []
            self.msg_file_structs = []
            self.exports = exports.copy()
            self.current_byte_offset = 0
            self.byte_length_calculation_callbacks = {}
            self.pBar.step()
            self.__load_msg_file(self.ros_msg_file, {}, {})
            self.pBar.step()
            self.__make_files()
            self.pBar.max()

    def __make_files(self):
        """
        Generates all the needed files

        :raises FileTypeNotSupportedException: when the filtype is not in SupportedExportTypes
        """
        xml_raw = self.__convert_to_xml()
        xml_string = minidom.parseString(Et.tostring(xml_raw)).toprettyxml(indent="\t")
        xml_string_clean = xml_to_cString(xml_string)

        msg_name = self.ros_msg_file.split(os.sep)[-1].replace(ROS_DOT_MSG_FILE, "")
        msg_header = get_hpp_header_from_msg_path(self.ros_msg_file, self.ros_src_path)
        msg_path_list = get_path_list_from_msg_path(self.ros_msg_file)[-3:]

        if not msg_header:
            raise Exception("Could not find header file to message file")

        if not msg_path_list:
            raise Exception("Could find path to message file")

        for key, value in self.exports.items():
            if key == self.SupportedExportTypes.json.value:

                filename = os.path.join(self.export_path, value)

                kwargs = {JSON_TAGS.MSG_NAME: msg_name,
                          JSON_TAGS.MSG_HEADER: msg_header,
                          JSON_TAGS.MSG_PATH_LIST: msg_path_list,
                          JSON_TAGS.MSG_DEPENDENCIES: self.dependencies,
                          JSON_TAGS.MSG_XML_STRING: xml_string,
                          JSON_TAGS.MSG_XML_STRING_CLEAN: xml_string_clean,
                          JSON_TAGS.MSG_DATA_PATHS: self.__make_data_path_list()}

                make_json_file(
                    filename,
                    **kwargs
                )

            elif key == self.SupportedExportTypes.pickle.value:

                filename = os.path.join(self.export_path, value)

                kwargs = {JSON_TAGS.MSG_NAME: msg_name,
                          JSON_TAGS.MSG_HEADER: msg_header,
                          JSON_TAGS.MSG_PATH_LIST: msg_path_list,
                          JSON_TAGS.MSG_DEPENDENCIES: self.dependencies,
                          JSON_TAGS.MSG_XML_STRING: xml_string,
                          JSON_TAGS.MSG_XML_STRING_CLEAN: xml_string_clean,
                          JSON_TAGS.MSG_DATA_PATHS: self.__make_data_path_list()}

                make_json_file(
                    filename,
                    **kwargs
                )

            elif key == self.SupportedExportTypes.xml.value:
                filename = os.path.join(self.export_path, value)
                make_file(filename, "\n", xml_string)

            elif key == self.SupportedExportTypes.txt.value:
                filename = os.path.join(self.export_path, value)
                make_file(filename, "\n", xml_string_clean)

            else:
                raise FileTypeNotSupportedException()

    def __parse_msg_file(self, parent_struct: MsgParentStruct, content_as_list: list, adtf_data_path: dict, ros_data_path: dict):
        """
        parses a msg file finds msg file if needed and opens it

        :param parent_struct: parent struct object
        :param content_as_list: content of msg file as
        :type content_as_list: list
        :param adtf_data_path: the path of the datastructure in adtf
        :type adtf_data_path: dict
        :param ros_data_path: the path of the datastructure in ros
        :type ros_data_path: dict
        :raises NotImplementedError: when const values detected in the msg file 
        :raises MsgAmbiguousException: when msg file was found to often  
        :raises MsgNotFoundException: when the msg file could not be located
        """
        clean_list = [x.split() for x in content_as_list]

        for values in clean_list:
            # get info of item
            # since the ros msg is build like this line by line
            # >>> fieldtype fieldname fielddefaultvalue this works
            # special case is a const value
            # >>> constanttype CONSTANTNAME=constantvalue
            # TODO add const value handling
            if len(values) < 2:
                continue
            if len(values) > 2:
                if not str(values[2]).startswith(ROS_COMMENT_INDICATOR):
                    default_value = values[2]  # Default Value is not used atm
            element_name = values[1]
            data_type = values[0]
            data_type_stripped = strip_type_name(data_type)
            array_size = get_array_size(data_type)

            # if element is a struct we recursivly parse the msg file of the struct if found
            if is_struct(data_type_stripped):

                # check if the element name represents a const val
                # if so raise exception becaujse const structs are nor allowed
                if is_const(element_name):
                    raise NotImplementedError(
                        "The datatype '%s' is a struct, the element name '%s' "
                        "represents a const value, structs cant be const!" % (data_type_stripped, element_name)
                    )

                # find msg file associated to struct
                msg_files = find_files(self.ros_src_path, data_type_stripped + ROS_DOT_MSG_FILE)

                # get struct info and append the data path
                if 1 < len(msg_files) < 3:
                    new_adtf_data_path = copy_append_dict(adtf_data_path, **{element_name: array_size})
                    new_ros_data_path = copy_append_dict(ros_data_path, **{element_name: array_size})
                    struct = MsgStruct(element_name, data_type_stripped, array_size=array_size)
                    parent_struct.add_element(struct)
                    self.byte_length_calculation_callbacks.update(
                        {data_type_stripped: struct.calculate_byte_length_callback}
                    )

                    # since we search in alphabetical order we take the msg file from the src folder by using idx -1
                    self.__load_msg_file(msg_files[0], new_adtf_data_path, new_ros_data_path)

                elif len(msg_files) > 2:
                    raise MsgAmbiguousException("The files: %s are ambiguous" % str(msg_files))
                else:
                    raise MsgNotFoundException("The file: %s could not be found" % str(msg_files))

            # just add the item as child to the struct
            else:

                if is_const(element_name):
                    raise NotImplementedError("The parsing of '%s' as a const value '%s' "
                                              "is not supported at the moment" % (data_type_stripped, element_name))

                supported_data_type = SupportedDataTypes.get_adtf_data_type(data_type_stripped)
                element = MsgElement(element_name,
                                     supported_data_type.adtf_data_type,
                                     supported_data_type.byte_length,
                                     array_size)

                element.add_data_paths(adtf_data_path, ros_data_path)
                parent_struct.add_element(element)
                self.current_byte_offset += element.byte_length

    def __load_msg_file(self, msg_file, adtf_data_path: dict, ros_data_path: dict):
        """
        load a msg file, get the content and forward it to the parser methdod

        :param msg_file: path to msg file
        :type msg_file: str pathlike
        :param adtf_data_path: the path of the datastructure in adtf
        :type adtf_data_path: dict
        :param ros_data_path: the path of the datastructure in ros
        :type ros_data_path: dict
        :raises FileNotFoundError:
        :raises MsgNotFoundException:
        :raises IOError:
        """
        try:
            path_to_file = Path(msg_file)

            # read file content
            with path_to_file.open() as file:
                content = file.readlines()
            content = [x.strip() for x in content]

            # remove comments
            if content:
                comments = find(lambda item: str(item).startswith(ROS_COMMENT_INDICATOR), content)
                if comments:
                    content = [x for x in content if x not in comments]

            # check result and append new struct
            if len(content) > 0:

                # check if the package is valid
                pkg = get_pkg_from_xml(msg_file)
                if not pkg:
                    raise FileNotFoundError("Could not locate package.xml for %s" % msg_file)
                if pkg not in self.dependencies:
                    self.dependencies.append(pkg)

                msg_file_list = msg_file.split(os.sep)
                struct_name = msg_file_list[-1].replace(ROS_DOT_MSG_FILE, "")
                struct = MsgParentStruct(struct_name)

                self.pBar.step()

                self.msg_file_structs.append(struct)
                self.__parse_msg_file(self.msg_file_structs[-1], content, adtf_data_path, ros_data_path)

                # run the byte calcualtion when the struct is finished and remove the callback from the list
                callback = self.byte_length_calculation_callbacks.get(struct_name)
                if callback is not None:
                    callback(struct)
                    del self.byte_length_calculation_callbacks[struct_name]
            else:
                raise MsgNotFoundException("The file: %s is invalid" % str(msg_file))

        except (FileNotFoundError, IOError, MsgNotFoundException):
            raise

    def __convert_to_xml(self):
        """
        starts the parsing of the generated list to xml
        :return: xml object
        """
        xml = Et.Element(DDL_TAGS.STRUCTS)
        self.msg_file_structs.reverse()
        for struct in self.msg_file_structs:
            self.__make_xml_sub(xml, struct)
        return xml

    def __make_xml_sub(self, parent, sub_element, byte_offset=0):
        """
        recursivly fills the given parent xml object
        :param parent: xml object to fill
        :param sub_element: struct or element
        :param byte_offset: only needed within recursion to append alements with right bytepos
        :return: byte_length of MsgElement or 0 if MsgStruct
        :rtype: int
        """
        if isinstance(sub_element, MsgParentStruct):
            struct = Et.SubElement(
                parent,
                DDL_TAGS.STRUCT,
                {DDL_TAGS.NAME: sub_element.name,
                 DDL_TAGS.VERSION: str(1),
                 DDL_TAGS.ALIGNMENT: str(1)}
            )
            offset = 0
            for element in sub_element.elements:
                offset += self.__make_xml_sub(struct, element, offset)
            return 0

        elif isinstance(sub_element, MsgElementBase):
            Et.SubElement(
                parent,
                DDL_TAGS.ELEMENT,
                {DDL_TAGS.NAME: sub_element.name,
                 DDL_TAGS.TYPE: sub_element.data_type,
                 DDL_TAGS.BYTEORDER: DDL_TAGS.LITTLE_ENDIAN,
                 DDL_TAGS.BYTEPOS: str(byte_offset),
                 DDL_TAGS.ALIGNMENT: str(1),
                 DDL_TAGS.ARRAYSIZE: str(sub_element.array_size)}
            )

            return sub_element.byte_length

    @staticmethod
    def __append_data_path_from_element(element_list: list, element):
        """
        for given element this method appends to given list a dictionary containing:
        {JSON_TAGS.DATA_TYPE: element.data_type,
        JSON_TAGS.ADTF_DATA_PATH: adtf_data_path,
        JSON_TAGS.ROS_DATA_PATH: ros_data_path}
        in respect to arraysize 
        
        :param element_list: list to append
        :type element_list: list
        :param element: item to process and apend
        :type element: class MsgElement
        """
        assert isinstance(element, MsgElement)

        element.make_data_path_list()

        # first check if the list is empty
        if len(element.adtf_data_path_as_list) > 0:

            # iterate over both lists at the same time
            for item1, item2 in zip(element.adtf_data_path_as_list, element.ros_data_path_as_list):  
                # check the array size
                if element.array_size > 1:

                    # append for each element in array
                    for i in range(element.array_size):
                        adtf_data_path = item1 + '.' + element.name + "[%i]" % i
                        ros_data_path = item2 + '.' + element.name + ".at(%i)" % i
                        element_list.append(
                            {JSON_TAGS.DATA_TYPE: element.data_type,
                             JSON_TAGS.ADTF_DATA_PATH: adtf_data_path,
                             JSON_TAGS.ROS_DATA_PATH: ros_data_path}
                        )

                # single element, arrasize ==1
                else:
                    element_list.append(
                        {JSON_TAGS.DATA_TYPE: element.data_type,
                         JSON_TAGS.ADTF_DATA_PATH: item1 + '.' + element.name,
                         JSON_TAGS.ROS_DATA_PATH: item2 + '.' + element.name}
                    )

        # data path list is empty element is not nested
        else:

            # check the array size
            if element.array_size > 1:

                # append for each element in array
                for i in range(element.array_size):
                    adtf_data_path = element.name + "[%i]" % i
                    ros_data_path = element.name + ".at(%i)" % i
                    element_list.append(
                        {JSON_TAGS.DATA_TYPE: element.data_type,
                         JSON_TAGS.ADTF_DATA_PATH: adtf_data_path,
                         JSON_TAGS.ROS_DATA_PATH: ros_data_path}
                    )

            # single element, arrasize ==1
            else:
                element_list.append(
                    {JSON_TAGS.DATA_TYPE: element.data_type,
                     JSON_TAGS.ADTF_DATA_PATH: element.name,
                     JSON_TAGS.ROS_DATA_PATH: element.name}
                )

    def __make_data_path_list(self):
        """
        generates all datapaths for every parsed struct element
        
        """
        result = []

        for struct in self.msg_file_structs:
            for element in struct.elements:
                if isinstance(element, MsgElement):
                    self.__append_data_path_from_element(result, element)

        return result
