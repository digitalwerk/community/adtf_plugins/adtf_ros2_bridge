import fnmatch
import os
import json
import shutil
import pickle
from enum import Enum
from colorama import Fore


def get_file_from_path(file_path):
    """
    trims a complete path to just the filename
    
    :param file_path: path to file
    :type file_path: str, pathlike
    :return: filename
    :rtype: str
    """
    return file_path.split(os.sep)[-1]


def copy_files(dst, *src, verbose=False):
    """
    helper function for file copy op
    
    :param dst: destination of copy operation
    :type dst: str, pathlike
    :param src: src file argument package
    :type src: str, pathlike
    :param verbose: verbose flag, defaults to False
    :param verbose: bool, optional
    """
    for s in src:
        shutil.copy2(s, dst)
        if verbose:
            print(Fore.GREEN + "File %s copied to: %s" % (s, dst))


def move_files(dst, *src, verbose=False, overwrite=True):
    """
    helper function for file move op
    
    :param dst: destination of copy operation
    :type dst: str, pathlike
    :param src: src file argument package
    :type src: str, pathlike
    :param verbose: verbose flag, defaults to False
    :param verbose: bool, optional
    :param overwrite: if true existing files will be overwritten, defaults to True
    :param overwrite: bool, optional
    """
    for s in src:

        if overwrite:
            d = os.path.join(dst, get_file_from_path(s))
        else:
            d = dst

        shutil.move(s, d)
        if verbose:
            print(Fore.GREEN + "File %s copied to: %s" % (s, d))


def list_to_pretty_string(l: list, sep=", "):
    """
    returns a list as string seperated by param sep
    
    :param l: [description]
    :type l: list
    :param sep: [description], defaults to ", "
    :param sep: str, optional
    """
    return sep.join(l)


def get_matching_lines_from_file(file, match, trim: iter=None):
    """
    Opens a file and returns all lines containing the give match pattern
    with thte trim param the result can be further processed

    :param file: fiile to open
    :type file: str, pathlike
    :param match: match pattern to look for in file lines
    :type match: str
    :param trim: trim pack, defaults to None
    :param trim: iter, optional
    :return: processed result or empty list
    :rtype: list
    """
    with open(file) as f:
        result = f.readlines()

    for line in result.copy():
        if match not in line:
            result.remove(line)

    if trim:
        for t in trim:
            result = [line.replace(t, "") for line in result]
    return result


def find(f, sequence: iter):
    """
    [summary]
    
    :param f: matching algorithm
    :type f: function
    :param sequence: iterable like list or dict.key, dict.value
    :type sequence: iter
    :return: first item in sequence where f(item) == True.
    :rtype: type like seqence item
    """
    for item in sequence:
        if f(item):
            return item


def create_dir_if_not_exist(directory):
    """
    creates directory if not exists
    
    :param directory: directory to create
    :type directory: str, pathlike
    """
    if not os.path.exists(directory):
        os.mkdir(os.path.join(directory))


def validate_path(path):
    """
    [summary]
    
    :param path: abspath to valdidate
    :type path: str, pathlike
    :raises FileNotFoundError: if path is invalid
    """
    if os.path.exists(path):
        return
    raise FileNotFoundError(path)


def check_file_ending(file, ending):
    """
    checks given file for the ending
    :param file: file to inspect
    :type file: str, pathlike
    :param ending: ending to match
    :type ending: str
    :raises Exception: if file does not end with ending
    """
    file_ending = file.split('.')[-1]
    if file_ending != ending:
        raise Exception("The file '%s' is not a *.%s file" % (file, ending))


def make_pickle_file(file, *args, **kwargs):
    """
    creates a pickle file with given args, kwargs and filename
    
    :param file: abspath to file, kargs
    :type file: str, pathlike
    :param args: argument pack with content
    :param kwargs: extended argument pack with content
    :raises Exception if file is is not a pkl or cant write to file
    :raises IOError if cant write to file
    """
    check_file_ending(file, "pkl")
    try:
        with open(file, "wb") as pkl_file:
            if args:
                pickle.dump(args, pkl_file)
            if kwargs:
                pickle.dump(kwargs, pkl_file)
    except IOError as e:
        print(e, file)
        raise e


def make_json_file(file, indent=4, *args, **kwargs):
    """
    creates a json file with given args, kwargs and filename
    
    :param file: abspath to file, kargs
    :type file: str, pathlike
    :param indent: indentation of json file, defaults to 4
    :type indent: int, optional
    :param args: argument pack with content
    :param kwargs: extended argument pack with content
    :raises Exception if file is is not a json or cant write to file
    :raises IOError if cant write to file
    """
    check_file_ending(file, "json")
    try:
        with open(file, 'w') as json_file:
            if args:
                json.dump(args, json_file, indent=indent)
            if kwargs:
                json.dump(kwargs, json_file, indent=indent)
    except IOError as e:
        print(e, file)
        raise e


def make_file(file, linesep, *args, **kwargs):
    """
    creates a file with given args, kwargs and filename
    
    :param file: abspath to file, kargs
    :type file: str, pathlike
    :param linesep: linesperator betweeen arguments
    :type linesep: str
    :param args: argument pack with content
    :param kwargs: extended argument pack with content
    :raises Exception if file is is not a json or cant write to file
    :raises IOError if cant write to file
    """
    try:
        with open(file, 'w') as f:
            for arg in args:
                f.write(arg)
                f.write(linesep)
            for key, value in kwargs.items():
                f.write(key)
                f.write(linesep)
                f.write(value)
                f.write(linesep)
    except IOError as e:
        print(e, file)
        raise e


def copy_append_dict(src_dict: dict, **kwargs):
    """
    Makes a copy of a dict and appends the items and retruns the new dict

    :param src_dict: source dictionary
    :type src_dict: dict
    :param kwargs: extended argument pack with content to update new dict with
    :return: new dict with updated items
    :rtype: dict
    """
    result = src_dict.copy()
    result.update(kwargs)
    return result


def find_files(path, file):
    """
    finds all files in a given path matching the given filename
    
    :param path: path to search
    :type path: str, pathlike
    :param file: name of file to search
    :type file: str
    :return: found matches
    :rtype: list
    """
    result = []
    for root, _, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, file):
                result.append((os.path.join(root, name)))
    return result


class ValueEnum(Enum):
    """
    just a simple class overwriting the __get__ magic method
    so always the value is returned
    """
    def __get__(self, instance, owner):
        return self.value
