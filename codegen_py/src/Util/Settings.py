from src.Util.Core import *
from src.Util.Singleton import *
from threading import Lock, Thread
import json


class SETTINGS_CONST(ValueEnum):
    """
    Store the used settings here
    """
    
    ROS_SRC_PATH = "ros_path"
    ADTF_SRC_PATH = "adtf_dir"
    VISUAL_STUDIO_BAT = "visual_studio_path"
    ADTF_PLUGIN_INSTALL_PATH = "adtf_plugin_install_path"
    ROS_SUBSCRIBER_PKG = "ros_subscriber_pkg"
    ROS_PUBLISHER_PKG = "ros_publisher_pkg"
    ROS_MSG_PKG = "ros_msg_pkg"
    ROS_BUILD_COMMAND = "ros_build_command"
    INIT_SETTINGS = {ROS_SRC_PATH: "path to your ros2 workspace",
                     ADTF_SRC_PATH: "path to your adtf installation",
                     ADTF_PLUGIN_INSTALL_PATH: "path to the dir where your adtf plugins will be installed to",
                     VISUAL_STUDIO_BAT: "path to your visual studio command line batch(only for windows)",
                     ROS_SUBSCRIBER_PKG: "adtf_generated_subscriber",
                     ROS_PUBLISHER_PKG: "adtf_generated_publisher",
                     ROS_MSG_PKG: "adtf_generated_msgs",
                     ROS_BUILD_COMMAND: "the command with arguments to build a ros2 pkg"}


class Settings(dict, metaclass=Singleton):
    """
    This class derived from the dictionary class
    implements a key based dictionary like global
    settings pattern to store and restore persistent values
    """
    _mtx = Lock()
    _filename = "settings.json"
    __slots__ = ("_file",)

    def __init__(self, filepath, init=SETTINGS_CONST.INIT_SETTINGS):
        """
        
        :param filepath: path to settings file
        :type filepath: str, pathlike
        :param init: initial settings, defaults to {}
        :param init: dict, optional
        """
        self._file = os.path.join(filepath, self._filename)
        try:
            validate_path(self._file)
            super().__init__(self._load())
        except FileNotFoundError:
            super().__init__(init)

    def __del__(self):
        pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._save()

    def __getitem__(self, key):
        return super().__getitem__(key)

    def __setitem__(self, key, value):
        super().__setitem__(key, value)

    def __delitem__(self, key):
        return super().__delitem__(key)

    def __contains__(self, item):
        return super().__contains__(item)

    def __repr__(self):
        return "{0}({1})".format(type(self).__name__, super().__repr__())

    def get(self, key, default=None):
        return super().get(key, default)

    def setdefault(self, key, default=None):
        return super().setdefault(key, default)

    def pop(self, key, default=None):
        if default:
            return super().pop(key, default)
        return super().pop(key)

    def update(self, mapping=(), **kwargs):
        self._mtx.acquire()
        try:
            super().update(mapping, **kwargs)
        finally:    
            self._mtx.release()

    def copy(self):
        return type(self)(self)

    @classmethod
    def fromkeys(cls, keys, value=None):
        return super().fromkeys(keys, value)

    def _load(self):
        try:
            with open(self._file, 'r') as json_file:
                data = json.load(json_file)
                return data
        except IOError:
            raise
        except Exception:
            raise

    def _save(self):
        make_json_file(self._file, **self)
