import tempfile
import os

class TempFileDistributor:
    """
    Dispatcher class for the safe use of temp files
    """
    USED_FILES = []

    @staticmethod
    def get_file():
        """
        Use this method to get a tempfile (filedescriptor, path)
        """
        tf, path = tempfile.mkstemp()
        TempFileDistributor.USED_FILES.append(path)
        return tf, path

    @staticmethod
    def clean_up():
        """
        Use this method to delete all used tempfiles
        """
        for path in TempFileDistributor.USED_FILES:
            os.remove(path)
