from src.templates.package_xml import *
from src.templates.subscriber_cpp import *
from src.templates.subscriber_h import *
from src.templates.publisher_cpp import *
from src.templates.publisher_h import *
from src.templates.cmake import *
from src.templates.batch import *


"""
This package is needed as storage for the strings that are needed for the codegenerator
"""