import os

CONTENT_UNIX = \
"""#!/bin/bash
cd /
. %s
cd %s
%s %s
"""

CONTENT_WIN = \
"""
call \"%s\"
call \"%s\"
cd /d \"%s\"
%s %s
"""


def get_batch(ros_setup, ros_src_path, ros_build_command, ros_pkg_name, vs_cmd_bat=None):
    """
    With given vars this function returns the content of a batch file
    
    :param vs_cmd_bat: path to visual studio batch, defaults to None
    :type vs_cmd_bat: str, pathlike, optional
    :param ros_setup: path to ros2 setup batch
    :type ros_setup: str, pathlike
    :param ros_src_path: path to ros2 src dir
    :type ros_src_path: str, pathlike
    :param ros_pkg_name: name of the package to build
    :type ros_pkg_name: str
    :param ros_build_command: name of ros2 build system like ament, catkin, colcon
    :type ros_build_command: str
    :return: content of bash/batch
    :rtype: str
    """
    if os.name == "nt":
        return CONTENT_WIN % (vs_cmd_bat, ros_setup, ros_src_path, ros_build_command, ros_pkg_name)
    else:
        return CONTENT_UNIX % (ros_setup, ros_src_path, ros_build_command, ros_pkg_name)
  
