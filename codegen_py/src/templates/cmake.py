CONTENT_SUBSCRIBER = \
r"""
#[[[cog
import cog
import json
import os

with open(export_file) as f:
    data = json.load(f)

pkg_name = data["pkg_name"]
msg_build_dependencies = data["msg_build_dependencies"]
adtf_path = data["adtf_path"].replace(os.sep, '/')
plugin_path = data["plugin_path"].replace(os.sep, '/')

cog.outl("cmake_minimum_required(VERSION 3.5)")  
cog.outl("project(" + pkg_name + ")")  
cog.outl("include(Macros.cmake)")  
cog.outl("if(NOT CMAKE_CXX_STANDARD)")  
cog.outl("\tset(CMAKE_CXX_STANDARD 14)")  
cog.outl("endif()")  
cog.outl("if(NOT WIN32)")  
cog.outl("\tset(CMAKE_CXX_FLAGS \"${CMAKE_CXX_FLAGS} -std=c++11 -w\")")  
cog.outl("endif()")  
cog.outl("find_package(ament_cmake REQUIRED)")  
for dep in msg_build_dependencies:
    cog.outl("find_package(" + dep + " REQUIRED)")
cog.outl("find_package(ADTF REQUIRED PATHS " + adtf_path + ")")  
cog.outl("find_package(ADTF COMPONENTS systemsdk)")  
cog.outl("find_package(ADTF COMPONENTS filtersdk)")  
cog.outl("")  
cog.outl("adtf_ros2_install_subscriber(" + pkg_name + " " + plugin_path)   
for dep in msg_build_dependencies:
    cog.outl("\t\"" + dep + "\"")
cog.outl(")")
cog.outl("ament_package()")  
]]]
#[[[end]]]
"""


CONTENT_PUBLISHER = \
r"""
#[[[cog
import cog
import json
import os

with open(export_file) as f:
    data = json.load(f)

pkg_name = data["pkg_name"]
msg_build_dependencies = data["msg_build_dependencies"]
adtf_path = data["adtf_path"].replace(os.sep, '/')
plugin_path = data["plugin_path"].replace(os.sep, '/')

cog.outl("cmake_minimum_required(VERSION 3.5)")  
cog.outl("project(" + pkg_name + ")")  
cog.outl("include(Macros.cmake)")  
cog.outl("if(NOT CMAKE_CXX_STANDARD)")  
cog.outl("\tset(CMAKE_CXX_STANDARD 14)")  
cog.outl("endif()")  
cog.outl("if(NOT WIN32)")  
cog.outl("\tset(CMAKE_CXX_FLAGS \"${CMAKE_CXX_FLAGS} -std=c++11 -w\")")  
cog.outl("endif()")  
cog.outl("find_package(ament_cmake REQUIRED)")  
for dep in msg_build_dependencies:
    cog.outl("find_package(" + dep + " REQUIRED)")
cog.outl("find_package(ADTF REQUIRED PATHS " + adtf_path + ")")  
cog.outl("find_package(ADTF COMPONENTS systemsdk)")  
cog.outl("find_package(ADTF COMPONENTS filtersdk)")  
cog.outl("")  
cog.outl("adtf_ros2_install_publisher(" + pkg_name + " " + plugin_path)   
for dep in msg_build_dependencies:
    cog.outl("\t\"" + dep + "\"")
cog.outl(")")
cog.outl("ament_package()")  
]]]
#[[[end]]]
"""


CONTENT_MSG = \
r"""
#[[[cog
import cog
import json
import os

with open(export_file) as f:
    data = json.load(f)

pkg_name = data["pkg_name"]
msg_build_dependencies = data["msg_build_dependencies"]
msg_files = data["msg_files"]

cog.outl("cmake_minimum_required(VERSION 3.5)")  
cog.outl("project(" + pkg_name + ")")  
cog.outl("if(NOT CMAKE_CXX_STANDARD)")  
cog.outl("\tset(CMAKE_CXX_STANDARD 14)")  
cog.outl("endif()")  
cog.outl("if(NOT WIN32)")  
cog.outl("\tset(CMAKE_CXX_FLAGS \"${CMAKE_CXX_FLAGS} -std=c++11 -w\")") 
cog.outl("endif()")   
cog.outl("")   
cog.outl("find_package(ament_cmake REQUIRED)")   
cog.outl("find_package(rosidl_default_generators REQUIRED)")   
for dep in msg_build_dependencies:
    cog.outl("find_package(" + dep + " REQUIRED)")
cog.outl("")   
cog.outl("rosidl_generate_interfaces(${PROJECT_NAME}")  
for msg in msg_files:
    cog.outl("\t\"msg/" + msg +  ".msg\"")
for dep in msg_build_dependencies:
    cog.outl("\tDEPENDENCIES " + dep)   
cog.outl(")")   
cog.outl("")   
cog.outl("ament_export_dependencies(rosidl_default_runtime)")  
cog.outl("ament_package()")  
]]]
#[[[end]]]
"""




FUNCTION_INSTALL_SUBSCRIBER = \
r"""function(adtf_ros2_install_subscriber TARGET INSTALL_PATH)

    set(SOURCES 
        src/adtf_ros2_subscriber_base.h
        src/${TARGET}.cpp 
        src/${TARGET}.h
    )
    adtf_add_plugin(${TARGET} ${SOURCES})
    target_link_libraries(${TARGET} adtf::systemsdk adtf::filtersdk)
    ament_target_dependencies(${TARGET} ${ARGN})
    
    if(WIN32)    
      set(PLUGIN_DESCRIPTION_GENERATOR adtf_plugin_description_generator.exe)
    else()    
      set(PLUGIN_DESCRIPTION_GENERATOR adtf_plugin_description_generator)
    endif()
    
    adtf_install_filter(${TARGET} ${INSTALL_PATH})
    add_custom_command(TARGET ${TARGET} POST_BUILD COMMAND ${ADTF_DIR}/bin/${PLUGIN_DESCRIPTION_GENERATOR} --plugin ${INSTALL_PATH}/${TARGET}.adtfplugin --output ${INSTALL_PATH}/${TARGET}.plugindescription)
endfunction()
"""

FUNCTION_INSTALL_PUBLISHER = \
r"""function(adtf_ros2_install_publisher TARGET INSTALL_PATH)
    set(SOURCES 
        src/adtf_ros2_publisher_base.h
        src/${TARGET}.cpp 
        src/${TARGET}.h
    ) 
    adtf_add_plugin(${TARGET} ${SOURCES})
    target_link_libraries(${TARGET} adtf::systemsdk adtf::filtersdk)
    ament_target_dependencies(${TARGET} ${ARGN})
    
    if(WIN32)    
      set(PLUGIN_DESCRIPTION_GENERATOR adtf_plugin_description_generator.exe)
    else()    
      set(PLUGIN_DESCRIPTION_GENERATOR adtf_plugin_description_generator)
    endif()
    
    adtf_install_filter(${TARGET} ${INSTALL_PATH})
    add_custom_command(TARGET ${TARGET} POST_BUILD COMMAND ${ADTF_DIR}/bin/${PLUGIN_DESCRIPTION_GENERATOR} --plugin ${INSTALL_PATH}/${TARGET}.adtfplugin --output ${INSTALL_PATH}/${TARGET}.plugindescription)
endfunction()
"""

FUNCTION_INSTALL_SERVICE = \
r"""function(adtf_ros2_install_service TARGET INSTALL_PATH)
    set(SOURCES 
        src/${TARGET}.cpp 
        src/${TARGET}.h
    )
    adtf_add_plugin(${TARGET} ${SOURCES})
    target_link_libraries(${TARGET} adtf::systemsdk adtf::filtersdk)
    ament_target_dependencies(${TARGET} ${ARGN})
    
    if(WIN32)    
      set(PLUGIN_DESCRIPTION_GENERATOR adtf_plugin_description_generator.exe)
    else()    
      set(PLUGIN_DESCRIPTION_GENERATOR adtf_plugin_description_generator)
    endif()
    
    adtf_install_service(${TARGET} ${INSTALL_PATH})
    add_custom_command(TARGET ${TARGET} POST_BUILD COMMAND ${ADTF_DIR}/bin/${PLUGIN_DESCRIPTION_GENERATOR} --plugin ${INSTALL_PATH}/${TARGET}.adtfplugin --output ${INSTALL_PATH}/${TARGET}.plugindescription)

endfunction()
"""