CONTENT_PUB_SUB = \
r"""<?xml version="1.0"?>
<!--
[[[cog
import cog
import json
with open(export_file) as f:
    data = json.load(f)

msg_build_dependencies = data["msg_build_dependencies"]
msg_exec_dependencies = data["msg_exec_dependencies"]
pkg_name = data["pkg_name"]
pkg_version = data["pkg_version"]
  
cog.outl("<package format=\"2\">")  
cog.outl("\t<name>" + pkg_name + "</name>")   
cog.outl("\t<version>" + pkg_version + "</version>") 
cog.outl("\t<description>Autogenerated ADTF package.</description>") 
cog.outl("\t<maintainer email=\"info@digitalwerk.net\">Autogenerated ADTF package</maintainer>") 
cog.outl("\t<license>Apache License 2.0</license>") 
cog.outl("")
cog.outl("\t<buildtool_depend>ament_cmake</buildtool_depend>")
cog.outl("\t<buildtool_depend>rosidl_default_generators</buildtool_depend>")
cog.outl("")
for dep in msg_build_dependencies:
    cog.outl("\t<build_depend>" + dep + "</build_depend>")
cog.outl("")
for dep in msg_exec_dependencies:
    cog.outl("\t<exec_depend>" + dep + "</exec_depend>")    
cog.outl("")  
cog.outl("\t<export>")  
cog.outl("\t\t<build_type>ament_cmake</build_type>")  
cog.outl("\t</export>")  
cog.outl("</package>")  
]]]-->
<!--[[[end]]]-->
"""

CONTENT_MSG = \
r"""<?xml version="1.0"?>
<!--
[[[cog
import cog
import json
with open(export_file) as f:
    data = json.load(f)

msg_build_dependencies = data["msg_build_dependencies"]
msg_exec_dependencies = data["msg_exec_dependencies"]
pkg_name = data["pkg_name"]
pkg_version = data["pkg_version"]

cog.outl("<package format=\"3\">")  
cog.outl("\t<name>" + pkg_name + "</name>")   
cog.outl("\t<version>" + pkg_version + "</version>") 
cog.outl("\t<description>Autogenerated ADTF package</description>") 
cog.outl("\t<maintainer email=\"info@digitalwerk.net\">Autogenerated ADTF package</maintainer>") 
cog.outl("\t<license>Apache License 2.0</license>") 
cog.outl("")
cog.outl("\t<buildtool_depend>ament_cmake</buildtool_depend>")
cog.outl("\t<buildtool_depend>rosidl_default_generators</buildtool_depend>")
cog.outl("")
for dep in msg_build_dependencies:
    cog.outl("\t<build_depend>" + dep + "</build_depend>")
cog.outl("")
for dep in msg_exec_dependencies:
    cog.outl("\t<exec_depend>" + dep + "</exec_depend>")    
cog.outl("")  
cog.outl("\t<member_of_group>rosidl_interface_packages</member_of_group>")
cog.outl("")    
cog.outl("\t<export>")  
cog.outl("\t\t<build_type>ament_cmake</build_type>")  
cog.outl("\t</export>")  
cog.outl("</package>")  
]]]-->
<!--[[[end]]]-->
"""