CONTENT = \
r"""
/**
 *
 * @file
 * Copyright &copy; Digitalwerk GmbH
 *
 */
/*[[[cog
import cog
import json

with open(export_file) as f:
    data = json.load(f)

stream = data["msg_name"]
cid_key = "CID_ROS2_PUBLISHER_" + stream.upper()
cid_value = stream.lower() + ".streaming_sink.adtf_ros2tb.cid"
type = '::'.join(data["msg_path_list"])
msg_header = "/".join(data["msg_header"])

ddl = data["msg_xml_string_clean"]

c_name = "cRos2Publisher" + stream
c =  "class " + c_name + " : public cRos2PublisherBase<msgType>"
c_macro = "ADTF_CLASS_ID_NAME(" + c_name + ", " + cid_key + ", \"ROS2 Publisher " + stream + "\");"
c_ctor = c_name + "();"
c_dtor = '~'+ c_name + "() = default;"
c_method_mapping  = "tVoid Mapping(msgType &oMsg) override;"
c_method_makeMap  = "tVoid MakeMap() override;"

cog.outl("#pragma once")
cog.outl("#include \"adtf_ros2_publisher_base.h\"")
cog.outl("#include \"%s\"" % msg_header)
cog.outl("#define %s \"%s\"" % (cid_key, cid_value))
cog.outl("typedef %s msgType;" % type)
cog.outl("")
cog.outl("constexpr const tChar* const g_strStreamName = \"%s\";" % stream)
cog.outl("constexpr const tChar* const g_strDDL =")
for line in ddl.split('\n'):
    cog.outl("%s" % line)
cog.outl(";")
cog.outl("")	
cog.outl(c)
cog.outl("{")
cog.outl("public:")
cog.outl("")
cog.outl("\t" + c_macro)
cog.outl("")
cog.outl("\t" + c_ctor)
cog.outl("\t" + c_dtor)
cog.outl("")
cog.outl("protected:")
cog.outl("\t" + c_method_mapping)
cog.outl("\t" + c_method_makeMap)
cog.outl("")
cog.outl("};")  
]]]*/
//[[[end]]]
"""