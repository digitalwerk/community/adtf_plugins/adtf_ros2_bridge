CONTENT = \
r"""
/*[[[cog
import cog
import json

with open(export_file) as f:
    data = json.load(f)

stream = data["msg_name"]
data_paths = data["msg_data_paths"]
include = "#include \"adtf_ros2_subscriber_" + stream.lower() + ".h\""
c_name = "cRos2Subscriber" + stream
c_macro = "ADTF_PLUGIN(\"ROS2 Subscriber " + stream + "\", " + c_name + ");"
c_ctor = c_name + "::" + c_name + "() : cRos2SubscriberBase(g_strStreamName, g_strDDL) {}"
c_method_mapping_head  = "tVoid " + c_name +"::Mapping(const cRos2SubscriberBase::msgSharedPtrType pMsg)"
c_method_makeMap_head  = "tVoid " + c_name +"::MakeMap()"

cog.outl(include)
cog.outl("")
cog.outl(c_macro)
cog.outl("")
cog.outl(c_ctor)
cog.outl("")
cog.outl(c_method_mapping_head)
cog.outl("{")
for d in data_paths:
    cog.outl("\t" + "m_oMap[\"" + d["adtf_data_path"] + "\"] = pMsg->" + d["ros_data_path"] + ";")	
cog.outl("}")
cog.outl("")
cog.outl(c_method_makeMap_head)
cog.outl("{")
for d in data_paths:
    cog.outl("\t" + "m_oMap.emplace(\"" + d["adtf_data_path"] + "\", " + d["data_type"] + "());")	
cog.outl("}")
]]]*/
//[[[end]]]    
"""