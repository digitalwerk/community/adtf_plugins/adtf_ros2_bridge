/**
 *
 * @file
 * Copyright &copy; Digitalwerk GmbH
 *
 */

#include "adtf_ros2_system_service.h"


ADTF_PLUGIN("ROS2 System Service", cRos2SystemService);

// Initialize ROS2
tResult cRos2SystemService::ServiceInit()
{
	rclcpp::init(0, nullptr);
	RETURN_NOERROR;
}

// ROS2 Cleanup
tResult cRos2SystemService::ServiceShutdown()
{
	rclcpp::shutdown();
	RETURN_NOERROR;
}