/**
 *
 * @file
 * Copyright &copy; Digitalwerk GmbH
 *
 */

#pragma once
#include <adtf_systemsdk.h>
#include "rclcpp/rclcpp.hpp"

using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::system;

#define CID_ROS2_ADTF_SYSTEM_SERVICE "ros2_com.service.adtf_ros2tb.cid"

class cRos2SystemService : public cADTFService 
{
public:

	ADTF_CLASS_ID_NAME(cRos2SystemService, CID_ROS2_ADTF_SYSTEM_SERVICE, "ROS2 System Service");

	tResult ServiceInit() override;
	tResult ServiceShutdown() override;

};
